#!/bin/bash

#SBATCH -J bg_no_w
#SBATCH -p normal
#SBATCH --array=1-2
#SBATCH -c 20
#SBATCH --mem 100G
#SBATCH -t 2879:00
#SBATCH -o ./temp/bundled_guidance/bg_no_w-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

## run bundled_guidance_reg_no_winsor_ln1.R for arrays=1-2
module load R/4.2.1
if [ ${SLURM_ARRAY_TASK_ID} -lt 3 ]
then
echo Rscript code/Rscript/bundled_guidance/bundled_guidance_reg_no_winsor_ln1.R ${SLURM_ARRAY_TASK_ID}
Rscript code/Rscript/bundled_guidance/bundled_guidance_reg_no_winsor_ln1.R ${SLURM_ARRAY_TASK_ID}

## run bundled_guidance_reg_no_winsor_g0.R for arrays=3-4
elif [ ${SLURM_ARRAY_TASK_ID} -ge 3 ]
then
NEW_TASK_ID=$((${SLURM_ARRAY_TASK_ID} - 2))
echo Rscript code/Rscript/bundled_guidance/bundled_guidance_reg_no_winsor_g0.R ${NEW_TASK_ID}
Rscript code/Rscript/bundled_guidance/bundled_guidance_reg_no_winsor_g0.R ${NEW_TASK_ID}
fi