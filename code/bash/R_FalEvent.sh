#!/bin/bash

#SBATCH -J 8K
#SBATCH -p normal
#SBATCH --array=1-3
#SBATCH -c 10
#SBATCH --mem 300G
#SBATCH -t 2879:00
#SBATCH -o ./temp/8K-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

indices='abc' # we are running separate jobs for letters a~c
echo $indices


letter=$( echo $indices | cut -c${SLURM_ARRAY_TASK_ID} )
echo We are running index $letter
module load R/4.2.1
echo Rscript code/Rscript/table8/R_FalEvent.R $letter
Rscript code/Rscript/table8/R_FalEvent.R $letter
