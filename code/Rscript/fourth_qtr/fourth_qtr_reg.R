################################################################################
## Script      : fourth_qtr_reg.R
## Name        : Eddie Yu
## Description : This script regresses ln_visits on post * bundled guidance variables
################################################################################


## Preamble, load necessary packages
rm(list = ls(all = TRUE))
library(data.table)
library(dplyr)
library(stargazer)
library(fixest)
library(car)
library(pastecs)
library(glue)
library(haven)
library(stringr)
library(statar)
library(DescTools)


## Preamble, set wd and load necessary packages
setwd('/zfs/projects/faculty/suzienoh-gps_proj/Eddie/gps_proj')
dataPath <- "./data/out/batch_NSZ_firm_abs_20211023_partial.csv"

number_of_rows = 100000
# df <- fread(dataPath, nrows=number_of_rows) # UNCOMMENT!
df <- fread(dataPath)

df[, `:=` (datadate = as.Date(datadate),
           sg_date  = as.Date(sg_date),
           eadate   = as.Date(eadate),
           ym_eadate_cor = 100*yr_eadate + m_eadate,
           yq_eadate_cor = 10*yr_eadate  + qtr_eadate,
           nai2 = naics_code %/% 10000)]

# filter data
df <- df[d_parent_ticker == 0,]
df <- df[between(aggday, -7, 6), ] # is this necessary? aggday already spans from -7 ~ 6

################################################################################
################################################################################
### REGRESSION
Xvar = "fourthqtr_dum"
outtex = glue("./out/tables/{Xvar}.tex")
print(outtex)

df <- as.data.frame(df)
clst = c("permno", "ym_eadate_cor") # cluster variables
X_labels = c(fourthqtr_dum="4th Quarter Indicator",
             lag_lossind="Lag Loss Ind", log_mve="Log(MVE)", lag_lev="Lag Lev",
             lag_btm="Lagged Book-to-Market", lag_capex_pct="Lagged Capex \\%", qbhr="Buy-Hold Abn Ret", 
             volatil="Volatility", ln_cov="Analyst Coverage")
setFixest_dict(c(ym_eadate_cor   = "Year-Month FE",
                 place_num       = "Store FE",
                 dow_sg_date     = "Day-of-Week FE",
                 fips_county_hyb = "County FE",
                 permno          = "Firm",
                 naics_code      = "Industry FE"))
setFixest_fml(
  ..interact = ~.[Xvar],
  ..ctrl = ~lag_lossind + log_mve + lag_lev + lag_btm + lag_capex_pct + qbhr  + volatil  + ln_cov,
  ..fe1 = ~place_num + ym_eadate_cor + dow_sg_date,
  ..fe2 = ~place_num + ym_eadate_cor^naics_code + dow_sg_date,
  ..fe3 = ~place_num + ym_eadate_cor^fips_county_hyb + dow_sg_date,
  ..fe4 = ~place_num + ym_eadate_cor^naics_code + ym_eadate_cor^fips_county_hyb + dow_sg_date,
  ..fe5 = ~place_num + ym_eadate_cor^naics_code + ym_eadate_cor^fips_county_hyb + permno^dow_sg_date,
  ..fe6 = ~place_num + ym_eadate_cor^naics_code + ym_eadate_cor^fips_county_hyb + naics_code^dow_sg_date)
order_cols = c("^post", Xvar)

reg1 <- feols(ln_visits_by_day_w01 ~ post*..interact + ..ctrl | ..fe1, df, cluster=clst, fixef.rm = "both")
reg2 <- feols(ln_visits_by_day_w01 ~ post*..interact + ..ctrl | ..fe2, df, cluster=clst, fixef.rm = "both")
reg3 <- feols(ln_visits_by_day_w01 ~ post*..interact + ..ctrl | ..fe3, df, cluster=clst, fixef.rm = "both")
reg4 <- feols(ln_visits_by_day_w01 ~ post*..interact + ..ctrl | ..fe4, df, cluster=clst, fixef.rm = "both")
reg5 <- feols(ln_visits_by_day_w01 ~ post*..interact + ..ctrl | ..fe5, df, cluster=clst, fixef.rm = "both")
reg6 <- feols(ln_visits_by_day_w01 ~ post*..interact + ..ctrl | ..fe6, df, cluster=clst, fixef.rm = "both")


## output main results
etable( reg1, reg2, reg3, reg4, reg5, reg6,
        file=outtex, replace = TRUE, digits = "r3", digits.stats = "r3",
        coefstat= "tstat", fitstat = ~ r2 + n, dict = X_labels, order=order_cols)

