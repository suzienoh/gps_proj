################################################################################
## Script      : desc_stat_notif_lag.R
## Name        : Eddie Yu
## Description : This script uses dataset "./data/out/nsz_earnnotif_lag.csv"
##                to run cross sectional test on variable "median_notif_lag_n6"
##               "./data/out/nsz_earnnotif_lag.csv" is constructed by 
##                code/Rscript/earn_notif/earn_notif_lag_construct.R
################################################################################



## Preamble, load necessary packages
rm(list = ls(all = TRUE))
library(data.table)
library(dplyr)
library(stargazer)
library(tidyverse)
library(fixest)
library(car)
library(glue)
library(haven)
library(vtable)

## Set working directory
setwd('/zfs/projects/faculty/suzienoh-gps_proj/Eddie/gps_proj')


# Configure Input/Output Filepaths
dataPath = "./data/out/nsz_earnnotif_lag.csv"

# Read data
number_of_rows = 100000
# df <- fread(dataPath, nrows = number_of_rows) # UNCOMMENT!
df <- fread(dataPath)


################################################################################
desc_vars = c("min_notif_lag", "max_notif_lag", "median_notif_lag")
Xvar = "median_notif_lag_n6"

### REGRESSION
df <- as.data.frame(df)
clst = c("permno", "ym_eadate_cor") # cluster variables
X_labels = c(median_notif_lag_n6="Above Industry Median (Lag) Earnings Notification",
             lag_lossind="Lag Loss Ind", log_mve="Log(MVE)", lag_lev="Lag Lev",
             lag_btm="Lagged Book-to-Market", lag_capex_pct="Lagged Capex \\%", qbhr="Buy-Hold Abn Ret", 
             volatil="Volatility", ln_cov="Analyst Coverage")

setFixest_dict(c(ym_eadate_cor   = "Year-Month FE",
                 place_num       = "Store FE",
                 dow_sg_date     = "Day-of-Week FE",
                 fips_county_hyb = "County FE",
                 permno          = "Firm",
                 naics_code      = "Industry FE"))
setFixest_fml(
  ..interact = ~.[Xvar],
  ..ctrl = ~lag_lossind + log_mve + lag_lev + lag_btm + lag_capex_pct + fourthqtr_dum  + qbhr  + volatil  + ln_cov,
  ..fe1 = ~place_num + ym_eadate_cor + dow_sg_date,
  ..fe2 = ~place_num + ym_eadate_cor^naics_code + dow_sg_date,
  ..fe3 = ~place_num + ym_eadate_cor^fips_county_hyb + dow_sg_date,
  ..fe4 = ~place_num + ym_eadate_cor^naics_code + ym_eadate_cor^fips_county_hyb + dow_sg_date,
  ..fe5 = ~place_num + ym_eadate_cor^naics_code + ym_eadate_cor^fips_county_hyb + permno^dow_sg_date,
  ..fe6 = ~place_num + ym_eadate_cor^naics_code + ym_eadate_cor^fips_county_hyb + naics_code^dow_sg_date)
order_cols = c("^post", Xvar)


reg1 <- feols(ln_visits_by_day_w01 ~ post * ..interact + ..ctrl | ..fe1, df, cluster=clst, fixef.rm = "both")


case.names.fixest <- function(object, ...){
  no <- object$obs_selection$obsRemoved
  return(seq_len(object$nobs_origin)[no])
}

esample_rowid = case.names(reg1) # e(sample) from reg1
df = setDT(df)
esample = df[esample_rowid,]

sumtable(esample[, ..desc_vars],
         add.median=T,
         summ.names=c('N','Mean','S.D', 'Min', 'Q1', 'Q2', 'Q3', 'Max'),
         out="latex",
         fit.page = '0.6\\textwidth',
         file=glue("./out/tables/earn_notif/desc_stats_earn_notif.tex"))
collapse_vars = c(c("gvkey", "datadate"), desc_vars)
esample_collapsed =
  unique(as.data.table(esample[, ..collapse_vars] %>%
                         group_by(gvkey, datadate)))
sumtable(esample_collapsed[, ..desc_vars],
         add.median=T,
         summ.names=c('N','Mean','S.D', 'Min', 'Q1', 'Q2', 'Q3', 'Max'),
         out="latex",
         title='Summary Statistics - Collapsed to Firm-Quarter level',
         fit.page = '0.6\\textwidth',
         file=glue("./out/tables/earn_notif/desc_stats_earn_notif_collapsed.tex"))
rm(reg1)

# exit script
quit(save = "no")
