# Eddie Repository

This directory is maintained by Eddie Yu working for NSZ (2023). Each directory is structured as follows.

## code
Contains Source code. Directory Rscript/ contains cleaned code, do/ contains STATA codes, bash/ contains Shell Scripts that will be submitted to run on Slurm Scheduler, to_erase stores source codes provided by Suzie (after review it can be removed). All .sh files should be run from Root Directory, Eddie/.

## data
Contains partial datasets that have been trimmed from CleanData/ directory. These .csv files are not heavy and can be run without using Yens. They have been produced from Eddie/code/save_partial_data.*

## out
Contains all the output of running source codes.

### out/tables
Contains all .tex files for replicating the paper's main results

## temp
This directory can be considered as a temporary folder that stores all outputs from running srun / sbatch jobs on Yens. It can be erased when we are producing a replication archive.

## License
All source codes follow Creative Commons (CC) license.