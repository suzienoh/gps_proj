#!/bin/bash

#SBATCH -J IR-reg
#SBATCH -p normal
#SBATCH --array=1-4
#SBATCH -c 10
#SBATCH --mem 100G
#SBATCH -t 200:00
#SBATCH -o ./temp/reg-IR-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
if [ ${SLURM_ARRAY_TASK_ID} == 1 ]
then
echo Rscript code/Rscript/IR/run_IR_keepinf_reg.R "T"
Rscript code/Rscript/IR/run_IR_keepinf_reg.R "T"

elif [ ${SLURM_ARRAY_TASK_ID} == 2 ]
then
echo Rscript code/Rscript/IR/run_IR_keepinf_reg.R "F"
Rscript code/Rscript/IR/run_IR_keepinf_reg.R "F"

elif [ ${SLURM_ARRAY_TASK_ID} == 3 ]
then
echo Rscript code/Rscript/IR/run_IR_dropinf_reg.R "T"
Rscript code/Rscript/IR/run_IR_dropinf_reg.R "T"

elif [ ${SLURM_ARRAY_TASK_ID} == 4 ]
then
echo Rscript code/Rscript/IR/run_IR_dropinf_reg.R "F"
Rscript code/Rscript/IR/run_IR_dropinf_reg.R "F"
fi