#!/bin/bash

#SBATCH -J notif_lag
#SBATCH -p normal
#SBATCH -c 20
#SBATCH --mem 80G
#SBATCH -t 2879:00
#SBATCH -o ./temp/earn-median-notif-lag-n6.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/earn_notif/reg_med_notif_lag_n6.R
Rscript code/Rscript/earn_notif/reg_med_notif_lag_n6.R