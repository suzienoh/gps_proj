#!/bin/bash

#SBATCH -J Ad_
#SBATCH -p normal
#SBATCH -c 15
#SBATCH --mem 1000G
#SBATCH -t 2879:00
#SBATCH -o ./temp/R_Ad.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/apndxC/R_Ad.R
Rscript code/Rscript/apndxC/R_Ad.R