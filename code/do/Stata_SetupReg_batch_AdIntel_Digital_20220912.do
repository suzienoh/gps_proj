

clear all
set maxvar   32767, permanently
set more off, permanently


use "/zfs/projects/faculty/suzienoh-gps_proj/CleanData/NSZ_firm_nabs_intel_digital_20220912.dta", clear   




rename *, lower 


gen yr_eadate = year(eadate)
gen qtr_eadate =quarter(eadate)
gen m_eadate = month(eadate)
gen yq_eadate = yr_eadate*10 + qtr_eadate
gen ym_eadate = yr_eadate*10 + m_eadate


****creating variables to be used as fixed effects in regressions****


gen double yqea_ind48 = yq_eadate*1000 + ff_48  /*for year-quarter X industry fixed effects*/
gen double ymea_ind48 = ym_eadate*1000 + ff_48  /*for year-quarter X industry fixed effects*/


gen double yqea_naic = yq_eadate*10000000 + naic  /*for year-quarter X industry fixed effects*/
gen double ymea_naic = ym_eadate*10000000 + naic  /*for year-quarter X industry fixed effects*/


gen double permno_yqact = yq_eadate*10000000  + permno /*for year-quarter X firm fixed effects*/
gen double permno_ymact = ym_eadate*10000000  + permno /*for year-quarter X firm fixed effects*/


egen place_num = group(safegraph_place_id)



gen double place_yqact = yq_eadate*100000000  + place_num /*for year-quarter X firm fixed effects*/
gen double place_ymact = ym_eadate*100000000  + place_num /*for year-quarter X firm fixed effects*/


 

egen region_num = group(region)

tabulate region
tabulate region_num, nolabel


gen double region_yqact = yq_eadate*1000  + region_num /*for year-quarter X region fixed effects*/
gen double region_ymact = ym_eadate*1000  + region_num /*for year-quarter X region fixed effects*/



egen city_num = group(city)


gen double city_yqact = yq_eadate*100000000  + city_num /*for year-quarter X region fixed effects*/
gen double city_ymact = ym_eadate*100000000  + city_num /*for year-quarter X region fixed effects*/



gen double fips_c_h_yqact = yq_eadate*100000000  + fips_county_hyb /*for year-quarter X region fixed effects*/
gen double fips_c_h_ymact = ym_eadate*100000000  + fips_county_hyb /*for year-quarter X region fixed effects*/

	


format yqea_ind48  ymea_ind48  yqea_naic  ymea_naic  permno_yqact  permno_ymact  place_yqact   place_ymact  region_yqact  region_ymact  city_yqact  city_ymact  fips_c_h_yqact  fips_c_h_ymact   %15.0g




cd "/zfs/projects/faculty/suzienoh-gps_proj/CleanData/"






 
 






/*********try logging daily visit counts!*************/

local ccgrp "visits_by_day_norm_cnty_hyb  visits_by_day " 
local n : word count `ccgrp'
forvalues i = 1/`n' {
local a : word `i' of `ccgrp'

gen ln_`a'=ln(1+`a')

}







replace cov=0  if missing(cov)
replace cov2=0  if missing(cov2)
 


local ccgrp "__67_90  __12_90   __16_90 __6_90  __1267_90" 
local n : word count `ccgrp'
forvalues i = 1/`n' {
local a : word `i' of `ccgrp'

replace cov`a'=0  if missing(cov`a')


gen ln_cov`a'=ln(1+cov`a')

}



gen ln_cov=ln(1+cov)
gen ln_cov2=ln(1+cov2)



gen all =1




 
 
 

****drop subsidiaries****	
drop if d_parent_ticker==1















global base /*shrout*/ qbhr  size_qbhr sim_ea illiquidity volatil insti inside inside_corr /*prccq*/ log_mve /*lossind */ lev btm capex_pct roa /*lag_lossind */ lag_lev lag_btm lag_capex_pct lag_roa /*fourthqtr_dum*/ /*friday*/  /*sg_date_friday*/  cov cov2 ln_cov__67_90  ln_cov__12_90  ln_cov__16_90  ln_cov__6_90 ln_cov__1267_90 ln_cov  ln_cov2  disp__67_90  disp__12_90  disp__16_90 disp__6_90  disp__1267_90   disp disp2 distance_from_home   median_dwell visits_per_visitor ufe ufe2 ufe__67_90  ufe__12_90   ufe__16_90 ufe__6_90  ufe__1267_90  medest medest2 medest__67_90  medest__12_90   medest__16_90 medest__6_90  medest__1267_90 actual actual2 actual__67_90  actual__12_90   actual__16_90 actual__6_90  actual__1267_90

drop sale_*

global vars1  sue1 sue2  ea_3_ret guide_newsqora   guide_newsq  /*guidanceq*/ /*indicator*/  guide_newsa   /*guidancea*/ /*indicator*/  /*bundled_guidance*/  /*indicator*/  full_count full_count_positive   factiva_count     num_eas      busyday altman_z     /* guide_news_rawq guide_news_rawa */  /*ufe__67_90 ufe__12_90 ufe__16_90 ufe__6_90 ufe__1267_90 ufe ufe2*/ /* For Media test:**/       abs_surp2__16_90 abs_surp2__6_90 abs_surp2__1267_90 abs_surp1 abs_surp2 abs_surp3 abs_surp4 abs_ea_3_ret   /**new**/ djnw_count  djnw_pos_count  djnw_neg_count  chg_altman_z /*chg_altman_z_yq_decile   chg_altman_z_yq_five  chg_altman_z_yq_two*/ fqchg_altman_z /*fqchg_altman_z_yq_decile  fqchg_altman_z_yq_five  fqchg_altman_z_yq_two*/  abs_surp2__67_90 abs_surp2_2  sur_er surp_er sue_er ssurp1_er ssurp2_er ssurp3_er sur_chr sue_chr  lag_sur_chr  lag_sue_chr lag4_sur_chr  lag4_sue_chr  /*sale_sup1 sale_sup6 sale_sup5 sale_sup4 sale_sup6_2 sale_sup5_2 sale_sup1__67_90 sale_sup6__67_90 sale_sup5__67_90  sale_sup4__67_90 sale_sup1__12_90 sale_sup6__12_90 sale_sup5__12_90 sale_sup4__12_90 sale_sup1__16_90 sale_sup6__16_90 sale_sup5__16_90 sale_sup4__16_90 sale_sup1__6_90 sale_sup6__6_90 sale_sup5__6_90 sale_sup4__6_90 sale_sup1__1267_90 sale_sup6__1267_90 sale_sup5__1267_90 sale_sup4__1267_90 */  spend_sum spend_sum_fill  number  number_fill sum_ea_spend_fill_d sum_ea_spend_d  sum_ea_number_fill_d  sum_ea_number_d sum_ea_spend_fill_w sum_ea_spend_w  sum_ea_number_fill_w sum_ea_number_w movavg3_spend_sum_fill  movavg3c_spend_sum_fill movavg3_number_fill movavg3c_number_fill digad_d_eff







winsor2   $base $vars1   surp*    , replace cuts(1 99) 





winsor2     visits_by_day   ln_visits_by_day     ,  suffix(_w001)  cuts(0.01 99.99) 
winsor2     visits_by_day   ln_visits_by_day   ,  suffix(_w01)  cuts(0.1 99.9) 
winsor2     visits_by_day   ln_visits_by_day    ,  suffix(_w1)  cuts(1 99) 
winsor2   visits_by_day_norm_cnty_hyb  ln_visits_by_day_norm_cnty_hyb    , replace cuts(1 99) 














cd "/zfs/projects/faculty/suzienoh-gps_proj/CleanData/"




local ccgrp "digad_d_eff " 
local n : word count `ccgrp'
forvalues i = 1/`n' {
local a : word `i' of `ccgrp'

gen `a'_f = .
replace `a'_f = `a' if !missing(`a')
replace `a'_f = 0 if missing(`a')

}


 
 
local ccgrp " spend_sum spend_sum_fill  number  number_fill sum_ea_spend_fill_d sum_ea_spend_d  sum_ea_number_fill_d  sum_ea_number_d sum_ea_spend_fill_w sum_ea_spend_w  sum_ea_number_fill_w sum_ea_number_w movavg3_spend_sum_fill  movavg3c_spend_sum_fill movavg3_number_fill movavg3c_number_fill  digad_d_eff digad_d_eff_f" 
local n : word count `ccgrp'
forvalues i = 1/`n' {
local a : word `i' of `ccgrp'

gen ln1_`a'=ln(1+`a')
gen ln_`a'=ln(`a')
}







sum digad_d_eff digad_d_eff_f ln1_digad_d_eff ln1_digad_d_eff_f ln_digad_d_eff ln_digad_d_eff_f




save batch_NSZ_firm_intel_dig_20220912, replace   /*save this for future Stata regressions using a smaller sample*/




	   
/***********************************************************
***************Producing file for R for Ad intel data********************************
************************************************************/

cd "/zfs/projects/faculty/suzienoh-gps_proj/Stata_Results/"



***************************************************************************
********creating "aggday" to capture days relative to the event date*******
********and keeping only 10 days before and after the event date***********
***************************************************************************

sort permno place_num eadate post sg_date  
by permno place_num eadate post: generate new_n = _n  /*first observation in pre starts from 1.*/  
replace new_n = new_n -1 if post==1 /*first observation in post starts from 0.*/ 



bysort permno place_num eadate post: egen post_max_new_n = max(new_n)


gen aggday = ((-1)*(post_max_new_n) + (new_n - 1)) if post==0

replace aggday = new_n if post==1

drop if aggday >= 11 
drop if aggday <= -11



keep if d_parent_ticker == 0










*****************************************************************
***************creating variables needed for regressions*********
*****************************************************************


**day of week variables and fixed effects**
gen dow_sg_date = dow(sg_date)
tabulate  dow_sg_date, gen(dow_fe)


**firm-datatdate(fiscal quarter end) fixed effects**
gen double permno_datadate = datadate*10000000  + permno 
format permno_datadate   %15.0g

   



**creating an interactive term to be used later in regressions**

gen  Xvar_surp = .
	   
	   
	   
	
	   
*****************************************************************
***************defining control variable list********************
*****************************************************************


	   
global control1  /*sg_date_friday*/
       
global control2 lag_lossind log_mve lag_lev lag_btm lag_capex_pct fourthqtr_dum lag_roa qbhr /**size_qbhr**/  illiquidity volatil insti inside ln_cov /*ln_cov__67_90  ln_cov__12_90  ln_cov__16_90  ln_cov__6_90 ln_cov__1267_90 ln_cov  ln_cov2*/  /*disp__67_90  disp__12_90  disp__16_90 disp__6_90  disp__1267_90   disp disp2*/
       

gen mve = exp(log_mve)




cd "/zfs/projects/faculty/suzienoh-gps_proj/CleanData/"


export delimited using batch_NSZ_firm_intel_dig_20220912.csv, replace

	   

	   
	   

	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   

	   	   
	   