#!/bin/bash

#SBATCH -J IR-save
#SBATCH -p normal
#SBATCH -c 20
#SBATCH --mem 1000G
#SBATCH -t 2879:00
#SBATCH -o ./temp/IR-save-%j.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/IR/IR_dropInf_save_specs.R
Rscript code/Rscript/IR/IR_dropInf_save_specs.R