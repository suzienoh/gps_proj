#!/bin/bash

#SBATCH -J IR-data
#SBATCH -p normal
#SBATCH -c 20
#SBATCH --mem 100G
#SBATCH -t 2879:00
#SBATCH -o ./temp/construct-IR-data.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/IR/construct_IR_dropinf_data.R
Rscript code/Rscript/IR/construct_IR_dropinf_data.R

echo Rscript code/Rscript/IR/construct_IR_keepinf_data.R
Rscript code/Rscript/IR/construct_IR_keepinf_data.R