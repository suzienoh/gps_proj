#!/bin/bash

#SBATCH -J dyn_reg
#SBATCH -p normal
#SBATCH --array=1-6
#SBATCH -c 4
#SBATCH --mem 90G
#SBATCH -t 240:00
#SBATCH -o ./temp/dyn/dyn-reg-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu


module load R/4.2.1
if [ ${SLURM_ARRAY_TASK_ID} -eq 1 ]
then
ntile="T"
days=14

elif [ ${SLURM_ARRAY_TASK_ID} -eq 2 ]
then
ntile="T"
days=21

elif [ ${SLURM_ARRAY_TASK_ID} -eq 3 ]
then
ntile="T"
days=28

elif [ ${SLURM_ARRAY_TASK_ID} -eq 4 ]
then
ntile="F"
days=14


elif [ ${SLURM_ARRAY_TASK_ID} -eq 5 ]
then
ntile="F"
days=21


elif [ ${SLURM_ARRAY_TASK_ID} -eq 6 ]
then
ntile="F"
days=28
fi



echo Rscript code/Rscript/dyn_reg/merge_sp_media_surp_dyn.R $ntile $days
Rscript code/Rscript/dyn_reg/merge_sp_media_surp_dyn.R $ntile $days