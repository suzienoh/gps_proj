#!/bin/bash

#SBATCH -J census2
#SBATCH -p normal
#SBATCH -c 5
#SBATCH --array=1-141
#SBATCH --mem 250G
#SBATCH -t 180:00
#SBATCH -o ./temp/census/run-census2-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/census/demographic_census2.R ${SLURM_ARRAY_TASK_ID} 
Rscript code/Rscript/census/demographic_census2.R ${SLURM_ARRAY_TASK_ID} 