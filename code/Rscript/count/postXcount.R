################################################################################
## Script      : postXcount.R
## Name        : Eddie Yu
## Description : This script regresses ln_visits on post * count variables (96 variables)
################################################################################


## Preamble, load necessary packages
rm(list = ls(all = TRUE))
library(data.table)
library(dplyr)
library(stargazer)
library(fixest)
library(car)
library(pastecs)
library(glue)
library(haven)
library(stringr)
library(statar)
library(DescTools)

# count variables
# count_cols = 
#   c("se", "se_naic", "se_sich", "se_two_sich", "se_two_naic", 
#     "se_four_naic", "se_5mve_yr", "se_10mve_yr", "se_20mve_yr", "se_25mve_yr",
#     "se_50mve_yr", "se_5mve_yq", "se_10mve_yq", "se_20mve_yq", "se_25mve_yq",
#     "se_50mve_yq", "se_5mve_ym", "se_10mve_ym", "se_20mve_ym", "se_25mve_ym",
#     "se_50mve_ym", "se_5mve_yr_naic", "se_10mve_yr_naic", "se_20mve_yr_naic",
#     "se_25mve_yr_naic", "se_50mve_yr_naic", "se_5mve_yr_sich", "se_10mve_yr_sich",
#     "se_20mve_yr_sich", "se_25mve_yr_sich", "se_50mve_yr_sich", 
#     "se_5mve_yr_two_naic" , "se_10mve_yr_two_naic", "se_20mve_yr_two_naic",
#     "se_25mve_yr_two_naic", "se_50mve_yr_two_naic", "se_5mve_yr_four_naic", 
#     "se_10mve_yr_four_naic" , "se_20mve_yr_four_naic", "se_25mve_yr_four_naic",
#     "se_50mve_yr_four_naic", "se_5mve_yr_two_sich", "se_10mve_yr_two_sich", 
#     "se_20mve_yr_two_sich", "se_25mve_yr_two_sich", "se_50mve_yr_two_sich", 
#     "se_5mve_yq_naic", "se_10mve_yq_naic", "se_20mve_yq_naic", "se_25mve_yq_naic",
#     "se_50mve_yq_naic", "se_5mve_yq_sich", "se_10mve_yq_sich", "se_20mve_yq_sich",
#     "se_25mve_yq_sich", "se_50mve_yq_sich", "se_5mve_yq_two_naic",
#     "se_10mve_yq_two_naic", "se_20mve_yq_two_naic", "se_25mve_yq_two_naic",
#     "se_50mve_yq_two_naic", "se_5mve_yq_four_naic", "se_10mve_yq_four_naic",
#     "se_20mve_yq_four_naic", "se_25mve_yq_four_naic" , "se_50mve_yq_four_naic",
#     "se_5mve_yq_two_sich" , "se_10mve_yq_two_sich", "se_20mve_yq_two_sich",
#     "se_25mve_yq_two_sich", "se_50mve_yq_two_sich", "se_5mve_ym_naic",
#     "se_10mve_ym_naic", "se_20mve_ym_naic", "se_25mve_ym_naic", "se_50mve_ym_naic",
#     "se_5mve_ym_sich" ,"se_10mve_ym_sich", "se_20mve_ym_sich", "se_25mve_ym_sich",
#     "se_50mve_ym_sich", "se_5mve_ym_two_naic", "se_10mve_ym_two_naic",
#     "se_20mve_ym_two_naic", "se_25mve_ym_two_naic", "se_50mve_ym_two_naic",
#     "se_5mve_ym_four_naic", "se_10mve_ym_four_naic" ,"se_20mve_ym_four_naic",
#     "se_25mve_ym_four_naic", "se_50mve_ym_four_naic", "se_5mve_ym_two_sich",
#     "se_10mve_ym_two_sich", "se_20mve_ym_two_sich", "se_25mve_ym_two_sich",
#     "se_50mve_ym_two_sich", "se_3day")
count_cols = 
  c("se_naic", "se_sich", "se_two_sich", 
    "se_four_naic", "se_5mve_yq_two_naic", "se_5mve_ym_two_naic")

# command line arguments for parallel processing 96 different count variables
args <- commandArgs(trailingOnly = TRUE)
varIndex <- as.numeric(args[1])
# varIndex <- 1 # UNCOMMENT LATER

Xvar = count_cols[varIndex]

## Preamble, set wd and load necessary packages
setwd('/zfs/projects/faculty/suzienoh-gps_proj/Eddie/gps_proj')

dataPath1 <- "./data/out/batch_NSZ_firm_abs_20211023_partial.csv"
dataPath2 <- "../../CleanData/compqa_ea_230925.csv"

number_of_rows = 100000
# ldf <- fread(dataPath1, nrows=number_of_rows) # UNCOMMENT LATER
ldf <- fread(dataPath1)
rdf <- fread(dataPath2, header=TRUE, stringsAsFactors = FALSE)

ldf[, `:=` (datadate = as.Date(datadate),
            sg_date  = as.Date(sg_date),
            eadate   = as.Date(eadate),
            ym_eadate_cor = 100*yr_eadate + m_eadate,
            nai2 = naics_code %/% 10000)]

rdf[, `:=` (datadate = as.Date(as.character(datadate), format="%Y%m%d"),
            rdq      = as.Date(as.character(rdq), format="%Y%m%d"))]
rdf_id_cols = c("gvkey", "datadate")
rdf_keep_cols = append(rdf_id_cols, count_cols)

# filter data
ldf <- ldf[d_parent_ticker == 0,]
ldf <- ldf[between(aggday, -7, 6), ] # is this necessary? aggday already spans from -7 ~ 6

# merge data
setkeyv(ldf, c("gvkey", "datadate"))
setkeyv(rdf, c("gvkey", "datadate"))
df <- merge(ldf, rdf[, ..rdf_keep_cols],
            by = c("gvkey", "datadate"),
            all.x = TRUE)
rm(ldf, rdf)

# change NA to 0 for count variale
fillNA = function(DT, cols=count_cols) {
  for (i in names(DT[, ..cols]))
    DT[is.na(get(i)), (i):=0]
}

fillNA(df)
# winsorize count variable
Xvar_w = paste0(Xvar, "_w")
quantile(df[, get(Xvar)], c(0, .01, .1, .9, .99, 1), na.rm=TRUE)
df[, paste0(Xvar, "_w") := Winsorize(get(Xvar), probs=c(0.01, 0.99), na.rm=T)]
quantile(df[, get(Xvar_w)], c(0, .01, .1, .9, .99, 1), na.rm=TRUE)

# take log
ln_count_w = paste0("ln_", Xvar_w)
if (varIndex > 4) {
  # df[, eval(ln_count_w) := log(1 + get(Xvar_w))]
  df[, eval(ln_count_w) := log(get(Xvar_w))]
} else {
  df[, eval(ln_count_w) := log(get(Xvar_w))]
}
summary(df[, get(ln_count_w)])




################################################################################
################################################################################
outtex <- glue("./out/tables/count/post_{ln_count_w}.tex")
print(outtex)
df <- as.data.frame(df)
# Run regression
clst = c("permno", "ym_eadate_cor") # cluster variables
setFixest_fml(
  ..interact = ~.[ln_count_w],
  ..ctrl = ~lag_lossind + log_mve + lag_lev + lag_btm + lag_capex_pct + fourthqtr_dum  + qbhr  + volatil  + ln_cov,
  ..fe1 = ~place_num + ym_eadate_cor + dow_sg_date,
  ..fe2 = ~place_num + ym_eadate_cor^naics_code + dow_sg_date,
  ..fe3 = ~place_num + ym_eadate_cor^fips_county_hyb + dow_sg_date,
  ..fe4 = ~place_num + ym_eadate_cor^naics_code + ym_eadate_cor^fips_county_hyb + dow_sg_date)
order_cols = c("^post", ln_count_w)

reg1 <- feols(ln_visits_by_day_w01 ~ post*..interact + ..ctrl | ..fe1, df, cluster=clst, fixef.rm = "both")

## store FE + naics_codeS*yearmonth FE + dow FE
reg2 <- feols(ln_visits_by_day_w01 ~ post*..interact + ..ctrl | ..fe2, df, cluster=clst, fixef.rm = "both")

## store FE + county*yearmonth FE + dow FE
reg3 <- feols(ln_visits_by_day_w01 ~ post*..interact + ..ctrl | ..fe3, df, cluster=clst, fixef.rm = "both")

## store + county*yearmonth + naics_codeS*yearmonth FE + dow FE
reg4 <- feols(ln_visits_by_day_w01 ~ post*..interact + ..ctrl | ..fe4, df, cluster=clst, fixef.rm = "both")


## output main results
etable( reg1, reg2, reg3, reg4, 
        file=outtex, replace = TRUE, digits = "r3", digits.stats = "r3",
        coefstat= "tstat", fitstat = ~ r2 + n, order=order_cols)