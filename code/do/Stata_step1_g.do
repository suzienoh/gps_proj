
clear all
set maxvar   32767, permanently
set more off, permanently

use "/zfs/projects/faculty/suzienoh-gps_proj/CleanData/NSZ_compqa_ea_qbhr_2022.dta", clear   




rename *, lower 


gen yr_eadate = year(eadate)
gen qtr_eadate =quarter(eadate)
gen m_eadate = month(eadate)
gen yq_eadate = yr_eadate*10 + qtr_eadate
gen ym_eadate = yr_eadate*10 + m_eadate /**not used due to error*/







cd "/zfs/projects/faculty/suzienoh-gps_proj/CleanData/"





gen all =1






global base  qbhr 



winsor2   $base   , replace cuts(1 99) 



cd "/zfs/projects/faculty/suzienoh-gps_proj/CleanData/"


save batch_NSZ_compqa_ea_qbhr_2022, replace 


export delimited using batch_NSZ_compqa_ea_qbhr_2022.csv, replace  


