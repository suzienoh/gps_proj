#!/bin/bash

#SBATCH -J r_attnt
#SBATCH -p normal
#SBATCH --array=1-36
#SBATCH -c 3
#SBATCH --mem 90G
#SBATCH -t 2879:00
#SBATCH -o ./temp/tweet/reg-tweet-attnt-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu


module load R/4.2.1
echo Rscript code/Rscript/tweet/reg_tweet_attnt.R ${SLURM_ARRAY_TASK_ID}
Rscript code/Rscript/tweet/reg_tweet_attnt.R ${SLURM_ARRAY_TASK_ID}

# if [ ${SLURM_ARRAY_TASK_ID} -le 18 ]
# then
# bool='F'
# NEW_TASK_ID=$((${SLURM_ARRAY_TASK_ID}))
# elif [ ${SLURM_ARRAY_TASK_ID} -gt 18 ]
# then
# bool='T'
# NEW_TASK_ID=$((${SLURM_ARRAY_TASK_ID} - 18))
# fi
