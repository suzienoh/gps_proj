#!/bin/bash

#SBATCH -J 4th
#SBATCH -p normal
#SBATCH -c 10
#SBATCH --mem 100G
#SBATCH -t 2879:00
#SBATCH -o ./temp/fourthqtr.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu


module load R/4.2.1
echo Rscript code/Rscript/fourth_qtr/fourth_qtr_reg.R
Rscript code/Rscript/fourth_qtr/fourth_qtr_reg.R