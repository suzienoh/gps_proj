#!/bin/bash

#SBATCH -J dyn_reg
#SBATCH -p normal
#SBATCH --array=1-16
#SBATCH -c 4
#SBATCH --mem 90G
#SBATCH -t 2879:00
#SBATCH -o ./temp/dyn/dyn-reg-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu


module load R/4.2.1
if [ ${SLURM_ARRAY_TASK_ID} -eq 1 ]
then
subsampling=1
bool=0
daily=0

elif [ ${SLURM_ARRAY_TASK_ID} -eq 2 ]
then
subsampling=1
bool=0
daily=1

elif [ ${SLURM_ARRAY_TASK_ID} -eq 3 ]
then
subsampling=1
bool=1
daily=0

elif [ ${SLURM_ARRAY_TASK_ID} -eq 4 ]
then
subsampling=1
bool=1
daily=1

elif [ ${SLURM_ARRAY_TASK_ID} -eq 5 ]
then
subsampling=2
bool=0
daily=0

elif [ ${SLURM_ARRAY_TASK_ID} -eq 6 ]
then
subsampling=2
bool=0
daily=1

elif [ ${SLURM_ARRAY_TASK_ID} -eq 7 ]
then
subsampling=2
bool=1
daily=0

elif [ ${SLURM_ARRAY_TASK_ID} -eq 8 ]
then
subsampling=2
bool=1
daily=1

elif [ ${SLURM_ARRAY_TASK_ID} -eq 9 ]
then
subsampling=3
bool=0
daily=0

elif [ ${SLURM_ARRAY_TASK_ID} -eq 10 ]
then
subsampling=3
bool=0
daily=1

elif [ ${SLURM_ARRAY_TASK_ID} -eq 11 ]
then
subsampling=3
bool=1
daily=0

elif [ ${SLURM_ARRAY_TASK_ID} -eq 12 ]
then
subsampling=3
bool=1
daily=1

elif [ ${SLURM_ARRAY_TASK_ID} -eq 13 ]
then
subsampling=4
bool=0
daily=0

elif [ ${SLURM_ARRAY_TASK_ID} -eq 14 ]
then
subsampling=4
bool=0
daily=1

elif [ ${SLURM_ARRAY_TASK_ID} -eq 15 ]
then
subsampling=4
bool=1
daily=0

elif [ ${SLURM_ARRAY_TASK_ID} -eq 16 ]
then
subsampling=4
bool=1
daily=1
fi


echo Rscript code/Rscript/dyn_reg/dyn_reg.R $subsampling $bool $daily
Rscript code/Rscript/dyn_reg/dyn_reg.R $subsampling $bool $daily