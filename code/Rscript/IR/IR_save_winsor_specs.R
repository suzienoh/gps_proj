################################################################################
## Script      : IR_save_winsor_specs.R
## Name        : Eddie Yu
## Description : This script (1) winsorizes surp6_2 at no winsorization + 6 different
##               specification levels, and then (2) drops missing surp6_@ obs, and then
##               (3) creates 12 different versions as in IR_save_specs.R
##
################################################################################


## Preamble, load necessary packages
rm(list = ls(all = TRUE))
library(data.table)
library(dplyr)
library(stargazer)
library(fixest)
library(car)
library(pastecs)
library(glue)
library(haven)
library(stringr)
library(statar)
library(DescTools)

## Set working directory
setwd("/zfs/projects/faculty/suzienoh-gps_proj/Eddie/gps_proj")
# dataPath1 <- "./data/batch_NSZ_firm_nabs_20211023_partial.csv" # original
dataPath1 <- "./data/Rstep2_batch_NSZ_firm_abs_20211023_partial.csv" # STATA converted code generated
number_rows = 4000000
# ldf <- fread(dataPath1, nrows = number_rows) # UNCOMMMENT LATER
ldf <- fread(dataPath1)


rdataPath <- "./data/IR_20221231.csv"
rdf <- fread(rdataPath) # IR data


ldf <- ldf[order(gvkey, eadate)]
ldf[, `:=` (nai2 = naics_code %/% 10000)]
ldf[, `:=` (gvkey = as.factor(gvkey),
            ym_eadate_cor = yr_eadate*100 + m_eadate,
            naics_code = as.factor(naics_code),
            nai2 = as.factor(nai2),
            fips_county_hyb = as.factor(fips_county_hyb),
            eadate = as.Date(eadate, format="%d%b%Y"),
            sg_date = as.Date(sg_date, format="%d%b%Y"),
            datadate = as.Date(datadate, format="%d%b%Y"))] 

# year-quarter, year-month
ldf[, `:=` (yr_datadate = year(datadate),
            qtr_datadate = quarter(datadate),
            wk_eadate = week(eadate),
            wk_datadate = week(datadate))]

ldf[, `:=` (yq_eadate_cor = 10*yr_eadate+qtr_eadate,
            yq_datadate_cor = 10*yr_datadate+qtr_datadate,
            ywk_eadate_cor = 100*yr_eadate+wk_eadate,
            ywk_datadate_cor = 100*yr_datadate+wk_datadate)]




rdf <- rdf[order(gvkey, fundq_qtr)]
rdf[, gvkey := as.factor(gvkey)]

# merge by datadate
print("Merging based on eadate & datadate, separately!")
df_datadate = merge(ldf, rdf,
                    by.x=c("gvkey", "yr_datadate", "qtr_datadate"),
                    by.y=c("gvkey", "fyearq", "fqtr"),
                    all.x=TRUE)
df_datadate[, IR := ifelse(is.na(IR), 0, IR)]

df_eadate = merge(ldf, rdf,
                  by.x=c("gvkey", "yr_eadate", "qtr_eadate"),
                  by.y=c("gvkey", "fyearq", "fqtr"),
                  all.x=TRUE)
df_eadate[, IR := ifelse(is.na(IR), 0, IR)]
print("Merging completed!")
rm(rdf, ldf)

# winsorize at different levels
var_to_winsorize = "surp6_2"
cuts = c(0.01, 0.99)
suffix = "_w"
df_datadate[, paste0(var_to_winsorize, suffix) := surp6_2]
df_eadate[, paste0(var_to_winsorize, suffix) := surp6_2]
merged_dfs = list(df_eadate) # list(df_eadate, df_datadate)
dfs_winsorized = list()
map_groups = list(
  c("yq_eadate_cor"),
  c("naics_code", "yq_eadate_cor"),
  c("nai2", "yq_eadate_cor"),
  c("ym_eadate_cor"), # different
  c("naics_code", "ym_eadate_cor"),
  c("nai2", "ym_eadate_cor") # different, same as above
)

# 14 dfs, 1-7 df_eadate winsorized at 6 levels / 8-14 df_datadate winsorized at 6 levels
for (i in 1:length(merged_dfs)) {
  dfs_winsorized <- append(dfs_winsorized, list(copy(merged_dfs[[i]])))
  
  for (j in 1:length(map_groups)) {
    df_temp = copy(merged_dfs[[i]]) # copy: pass by value, NOT pass-by-reference
    grp = map_groups[[j]]
    
    print(glue("Creating variable: {paste0(var_to_winsorize, suffix)}"))
    print(glue("Group By: {grp}"))
    df_temp[, paste0(var_to_winsorize, suffix) := NULL]
    df_temp[, paste0(var_to_winsorize, suffix) := Winsorize(get(var_to_winsorize),
                                                            probs = cuts, 
                                                            na.rm=TRUE),
            by=grp]
    last_column <- df_temp[is.finite(get(paste0(var_to_winsorize, suffix))), get(paste0(var_to_winsorize, suffix))]
    print(summary(last_column))
    dfs_winsorized <- append(dfs_winsorized, list(copy(df_temp)))
    rm(df_temp)
  }
}
rm(merged_dfs)

# toy = distinct(df_eadate[!is.na(surp6_2), .(gvkey, surp6_2, yq_eadate_cor, naics_code, nai2)])
# toy[, paste0(var_to_winsorize, "_yq_w") := Winsorize(surp6_2,probs = cuts, na.rm=TRUE),
#           by=c("yq_eadate_cor")]
# 
# toy[, paste0(var_to_winsorize, "_nai6_yq_w") := Winsorize(surp6_2,probs = cuts, na.rm=TRUE),
#     by=c("naics_code", "yq_eadate_cor")]

# # toy example
# a = c(-Inf, 1, 1, 1, 1, 1, 1, 2, 3, 99, NA, 99, 99.9, Inf, 101)
# b = c("T", "T", "T", "T", "T", "T", "T", "T", "T", "F", "F", "F", "F", "F", "F")
# c = c("A", "A", "A", "A",  "B", "B", "B", "B", "B", "A", "A", "A", "B", "B", "B")
# f = data.table(a, b, c)
# f[, n := Winsorize(a, probs = c(0.01, 0.99), na.rm=T), by=.(b, c)]
# quantile(f[b=="T"]$a, c(0, .01 ,.99, 1), na.rm=TRUE)
# quantile(f$a, c(0, .01 ,.99, 1), na.rm=TRUE)


# tenure restrictions for 14 dfs_winsorized
variable = "surp6_2"
tenure_restriction = 4 # 4 quarters, 12 quarters
dfs_winsorized_tenure = list()

for (i in 1:length(dfs_winsorized)) {
  
  print(glue("Tenure Restriction: {tenure_restriction} quarters"))
  df_temp = copy(dfs_winsorized[[i]])
  
  # if (i<=6) { ##df_eadate
  df_temp[IR==1, IR_sum := .GRP, by = .(gvkey, yr_eadate, qtr_eadate)]
  
  # } else if (i>6 & i <= 12) { ##df_datadate
  #   df_temp[IR==1, IR_sum := .GRP, by = .(gvkey, yr_datadate, qtr_datadate)]
  # } else {
  #   stop("IndexError: df_eadate, df_datadate indexing out of bounds")
  # }
  
  df_temp[, IR_sum := ifelse(!is.na(IR_sum), IR_sum, 0)]
  df_temp[, IR_tenure_i := as.numeric(IR_sum >= tenure_restriction)]
  
  # drop missing surp6_2
  df_temp <- df_temp[!is.na(get(variable)),]
  print(summary(df_temp[is.finite(surp6_2), .(gvkey, IR_tenure_i, surp6_2)]))
  dfs_winsorized_tenure <- append(dfs_winsorized_tenure, list(copy(df_temp)))
  rm(df_temp)
  
}

rm(dfs_winsorized)


# create 3 quintile indicators: Year-Month, Year-Quarter, Year-Week
num_bins = 5 # number of bins
variable = "surp6_2_w"
prefixes = c("ym_", "yq_", "ywk_")
groups = list(c("ym_eadate_cor"), c("yq_eadate_cor"), c("ywk_eadate_cor"))
# groups = list(c("ym_datadate_cor"), c("yq_datadate_cor"), c("ywk_datadate_cor"))
dfs_winsorized_quintiles = list()

for (df_index in 1:length(dfs_winsorized_tenure)) {
  df_temp = copy(dfs_winsorized_tenure[[df_index]])
  
  # make 2 copies, for xtile and ntile 
  df_temp_xtile = copy(df_temp)
  df_temp_ntile = copy(df_temp)
  
  for (i in 1:length(groups)) {
    prefix = prefixes[i]
    grp = groups[[i]]
    print(glue("Prefix: {prefix}"))
    print(glue("Group By: {grp}"))
    
    print("Creating stata xtile, R ntile bins..")
    df_temp_xtile[, paste0(prefix, variable) := statar::xtile(get(variable), num_bins),
                  by=grp]
    
    # df_temp_xtile <- setDT(df_temp_xtile %>% 
    #                          group_by_at(.vars=grp) %>% 
    #                          mutate(new_var = statar::xtile(get(variable), 
    #                                                                num_bins)))
    
    
    
    df_temp_ntile[, paste0(prefix, variable) := ntile(get(variable), num_bins),
                  by=grp]
    # df_temp_ntile <- setDT(df_temp_ntile %>% 
    #                          group_by_at(.vars=grp) %>% 
    #                          mutate(new_var = ntile(get(variable), 
    #                                                        num_bins)))
    print(glue("All Rows: {nrow(df_temp_xtile)} / Finite Rows : {nrow(df_temp_xtile[is.finite(get(variable)),])}"))
    print(summary(df_temp_xtile[is.finite(get(variable)), get(paste0(prefix, variable))]))
    print(summary(df_temp_ntile[is.finite(get(variable)), get(paste0(prefix, variable))]))
    
  }
  
  
  dfs_winsorized_quintiles <- append(dfs_winsorized_quintiles, list(copy(df_temp_xtile)))
  print(glue("Length of iterables: {length(dfs_winsorized_quintiles)}"))
  dfs_winsorized_quintiles <- append(dfs_winsorized_quintiles, list(copy(df_temp_ntile)))
  print(glue("Length of iterables: {length(dfs_winsorized_quintiles)}"))
  
  rm(df_temp, df_temp_xtile, df_temp_ntile)
}

rm(dfs_winsorized_tenure)



# filter out relevant variables
df_iterables_final = list()
for (df_index in 1:length(dfs_winsorized_quintiles)) {
  df_temp = copy(dfs_winsorized_quintiles[[df_index]])
  df_temp <- df_temp[d_parent_ticker==0,]
  df_temp <- df_temp[between(aggday, -7, 6), ]
  df_temp <- df_temp[, .(gvkey, eadate, datadate, sg_date, 
                         ym_eadate_cor, yq_eadate_cor, ywk_eadate_cor,
                         # ym_datadate_cor, yq_datadate_cor, ywk_datadate_cor, 
                         naics_code, nai2,
                         surp6_2, surp6_2_w, ym_surp6_2_w, yq_surp6_2_w, ywk_surp6_2_w,
                         IR, IR_sum, IR_tenure_i, post, ln_visits_by_day_w01,
                         lag_lossind, log_mve, lag_lev, lag_btm,
                         lag_capex_pct, fourthqtr_dum, qbhr, volatil, ln_cov,
                         place_num, fips_county_hyb, dow_sg_date, permno)]
  df_iterables_final <- append(df_iterables_final, list(copy(df_temp)))
  print(glue("Iterable Dataframes: {length(df_iterables_final)}"))
}
rm(dfs_winsorized_quintiles)

saveFilePaths = c()
saveFilePrefix = "./data/out/IR_"
# matches = c("eadate", "datadate")
# tenures = c("_tenure0_", "_tenure4_", "_tenure12_")

# Each of 14 files has surp6_2_w, and also quintiles grouped in 3 manners (ym_surp6_2_w, yq_surp6_2_w, ywk_surp6_2_w)
# so we can run reg on 4 
winsors = c("w0_")
winsors = append(winsors, paste0("w_", c( "yq_", "nai6_yq_", "nai2_yq_", "ym_", "nai6_ym_", "nai2_ym_")))
tiles   = c("xtile.csv", "ntile.csv")
for (tile in tiles) {
  for (winsor in winsors) {
    outPath = paste0(saveFilePrefix, winsor, "t", tenure_restriction, "_", tile)
    print(outPath)
    saveFilePaths <- append(saveFilePaths, outPath)
  }
}

for (fileIndex in 1:length(saveFilePaths)) {
  file = copy(df_iterables_final[[fileIndex]])
  path = saveFilePaths[fileIndex]
  print(glue("Writing to {path}.."))
  fwrite(file, path) # UNCOMMENT LATER
  print("..Done!")
}

rm(list=ls())
gc()