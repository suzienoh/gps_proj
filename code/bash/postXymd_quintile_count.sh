#!/bin/bash

#SBATCH -J ymdquint
#SBATCH -p normal
#SBATCH --array=1-6
#SBATCH -c 10
#SBATCH --mem 70G
#SBATCH -t 2879:00
#SBATCH -o ./temp/count/ymdquint-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/count/postXymd_quintile_count_reg.R ${SLURM_ARRAY_TASK_ID}
Rscript code/Rscript/count/postXymd_quintile_count_reg.R ${SLURM_ARRAY_TASK_ID}