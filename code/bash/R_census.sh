#!/bin/bash

#SBATCH -J R_census
#SBATCH -p normal
#SBATCH -c 10
#SBATCH --mem 200G
#SBATCH -t 2879:00
#SBATCH -o ./temp/R_census.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/table6/R_census_hardcodeFE.R
Rscript code/Rscript/table6/R_census_hardcodeFE.R
