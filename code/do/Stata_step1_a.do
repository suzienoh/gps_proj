
clear all
set maxvar   32767, permanently
set more off, permanently



use "/zfs/projects/faculty/suzienoh-gps_proj/CleanData/NSZ_yFIP_10_firm_nabs_20211023.dta", clear   





rename *, lower 


gen yr_eadate = year(eadate)
gen qtr_eadate =quarter(eadate)
gen m_eadate = month(eadate)
gen yq_eadate = yr_eadate*10 + qtr_eadate
gen ym_eadate = yr_eadate*10 + m_eadate /**not used due to error*/




gen double yqea_naic = yq_eadate*10000000 + naic  
gen double ymea_naic = ym_eadate*10000000 + naic 


gen double permno_yqact = yq_eadate*10000000  + permno 
gen double permno_ymact = ym_eadate*10000000  + permno 


egen place_num = group(safegraph_place_id)



gen double place_yqact = yq_eadate*100000000  + place_num 
gen double place_ymact = ym_eadate*100000000  + place_num 


 

egen region_num = group(region)

tabulate region
tabulate region_num, nolabel


gen double region_yqact = yq_eadate*1000  + region_num 
gen double region_ymact = ym_eadate*1000  + region_num 



egen city_num = group(city)


gen double city_yqact = yq_eadate*100000000  + city_num 
gen double city_ymact = ym_eadate*100000000  + city_num 



gen double fips_c_h_yqact = yq_eadate*100000000  + fips_county_hyb 
gen double fips_c_h_ymact = ym_eadate*100000000  + fips_county_hyb 

	


format  yqea_naic  ymea_naic  permno_yqact  permno_ymact  place_yqact   place_ymact  region_yqact  region_ymact  city_yqact  city_ymact  fips_c_h_yqact  fips_c_h_ymact   %15.0g




cd "/zfs/projects/faculty/suzienoh-gps_proj/CleanData/"




 

local ccgrp "visits_by_day_norm_cnty_hyb  visits_by_day " 
local n : word count `ccgrp'
forvalues i = 1/`n' {
local a : word `i' of `ccgrp'

gen ln_`a'=ln(1+`a')

}







replace cov=0  if missing(cov)
replace cov2=0  if missing(cov2)
 


local ccgrp "__67_90  __12_90   __16_90 __6_90  __1267_90" 
local n : word count `ccgrp'
forvalues i = 1/`n' {
local a : word `i' of `ccgrp'

replace cov`a'=0  if missing(cov`a')


gen ln_cov`a'=ln(1+cov`a')

}



gen ln_cov=ln(1+cov)
gen ln_cov2=ln(1+cov2)



gen all =1




 
 
 
drop if d_parent_ticker==1 /**************using parent or stand-alone companies only*****************/














global base /*shrout*/ qbhr  size_qbhr sim_ea illiquidity volatil insti inside inside_corr /*prccq*/ log_mve /*lossind */ lev btm capex_pct roa /*lag_lossind */ lag_lev lag_btm lag_capex_pct lag_roa /*fourthqtr_dum*/ /*friday*/  /*sg_date_friday*/  cov cov2 ln_cov__67_90  ln_cov__12_90  ln_cov__16_90  ln_cov__6_90 ln_cov__1267_90 ln_cov  ln_cov2  disp__67_90  disp__12_90  disp__16_90 disp__6_90  disp__1267_90   disp disp2 distance_from_home   median_dwell visits_per_visitor ufe ufe2 ufe__67_90  ufe__12_90   ufe__16_90 ufe__6_90  ufe__1267_90  medest medest2 medest__67_90  medest__12_90   medest__16_90 medest__6_90  medest__1267_90 actual actual2 actual__67_90  actual__12_90   actual__16_90 actual__6_90  actual__1267_90

global vars1  sue1 sue2  ea_3_ret guide_newsqora   guide_newsq  /*guidanceq*/ /*indicator*/  guide_newsa   /*guidancea*/ /*indicator*/  /*bundled_guidance*/  /*indicator*/  full_count full_count_positive   factiva_count     num_eas      busyday altman_z     /* guide_news_rawq guide_news_rawa */  /*ufe__67_90 ufe__12_90 ufe__16_90 ufe__6_90 ufe__1267_90 ufe ufe2*/ /* For Media test:**/       abs_surp2__16_90 abs_surp2__6_90 abs_surp2__1267_90 abs_surp1 abs_surp2 abs_surp3 abs_surp4 abs_ea_3_ret   /**new**/ djnw_count  djnw_pos_count  djnw_neg_count  chg_altman_z /*chg_altman_z_yq_decile   chg_altman_z_yq_five  chg_altman_z_yq_two*/ fqchg_altman_z /*fqchg_altman_z_yq_decile  fqchg_altman_z_yq_five  fqchg_altman_z_yq_two*/  abs_surp2__67_90 abs_surp2_2  sur_er surp_er sue_er ssurp1_er ssurp2_er ssurp3_er sur_chr sue_chr  lag_sur_chr  lag_sue_chr lag4_sur_chr  lag4_sue_chr  sale_sup1 sale_sup6 sale_sup5 sale_sup4 sale_sup6_2 sale_sup5_2 sale_sup1__67_90 sale_sup6__67_90 sale_sup5__67_90  sale_sup4__67_90 sale_sup1__12_90 sale_sup6__12_90 sale_sup5__12_90 sale_sup4__12_90 sale_sup1__16_90 sale_sup6__16_90 sale_sup5__16_90 sale_sup4__16_90 sale_sup1__6_90 sale_sup6__6_90 sale_sup5__6_90 sale_sup4__6_90 sale_sup1__1267_90 sale_sup6__1267_90 sale_sup5__1267_90 sale_sup4__1267_90 



global vars2  abs_ea_3_ret abs_sue1 abs_sue2 abs_guide_newsqora  abs_guide_newsq  abs_guide_newsa      abs_guide_news_rawq abs_guide_news_rawa        /* abs_ufe__67_90 abs_ufe__12_90 abs_ufe__16_90 abs_ufe__6_90 abs_ufe__1267_90 abs_ufe abs_ufe2*/



 




global census1  i15u1_r i20u1_r i25u1_r i30u1_r  i35u1_r i40u1_r i50u1_r i75u1_r i125u1_r  i200u1_r i10d1_r i15d1_r i20d1_r i25d1_r i30d1_r i35d1_r i40d1_r i45d1_r i50d1_r i60d1_r i75d1_r   i100d1_r i125d1_r i150d1_r i200d1_r i15_150m1_r i15_125m1_r  i15_100m1_r i15_75m1_r i15_60m1_r i15_50m1_r  i15_45m1_r i15_40m1_r i20_150m1_r i20_125m1_r i20_100m1_r  i20_75m1_r  i20_60m1_r  i20_50m1_r  i20_45m1_r  i20_40m1_r  i25_150m1_r  i25_125m1_r  i25_100m1_r  i25_75m1_r  i25_60m1_r  i25_50m1_r i25_45m1_r i30_150m1_r i30_125m1_r i30_100m1_r i30_75m1_r i30_60m1_r i30_50m1_r i30_45m1_r i35_150m1_r i35_125m1_r i35_100m1_r i35_75m1_r i35_60m1_r i35_50m1_r i40_150m1_r i40_125m1_r i40_100m1_r i40_75m1_r i40_60m1_r i45_150m1_r i45_125m1_r i45_100m1_r i45_75m1_r i45_60m1_r i50_150m1_r i50_125m1_r  i50_100m1_r i50_75m1_r i60_150m1_r  i60_125m1_r i60_100m1_r i60_75m1_r i75_150m1_r i75_125m1_r i75_100m1_r i100_150m1_r i100_125m1_r  a75u_r a70u_r  a65u_r a55u_r a19b_r a21b_r a24b_r a29b_r a34b_r a39b_r a44b_r a49b_r a54b_r a59b_r  a61b_r a64b_r  d7b_r  d8b_r d9b_r d10b_r d11b_r d12b_r  dhb_r  dgedb_r   dscb_r  i10d2_r i15d2_r i20d2_r  i25d2_r i30d2_r i35d2_r i40d2_r i45d2_r  i50d2_r i60d2_r i75d2_r i100d2_r i125d2_r i150d2_r i200d2_r a3040_r a3050_r a3070_r a3570_r a3560_r a3550_r a3540_r days_sincefqe eaorder1 eaorder2 eaorder1_all eaorder1_all_ind12 eaorder1_ind12 eaorder2_all eaorder2_all_ind12 eaorder2_ind12 /*patternfirmqtr*/ tradingays_sincefqe weekdays_sincefqe fqchg_days_sincefqe fqchg_eaorder1 fqchg_eaorder2 fqchg_eaorder1_all fqchg_eaorder1_all_ind12 fqchg_eaorder1_ind12 fqchg_eaorder2_all fqchg_eaorder2_all_ind12 fqchg_eaorder2_ind12  fqchg_tradingays_sincefqe fqchg_weekdays_sincefqe eaorder1_inc eaorder1_all_inc fqchg_eaorder1_inc fqchg_eaorder1_all_inc  eaorder2_inc eaorder2_all_inc fqchg_eaorder2_inc  fqchg_eaorder2_all_inc eaorder2_ind48 eaorder2_all_ind48 fqchg_eaorder2_ind48 fqchg_eaorder2_all_ind48  seaorder1 seaorder2 seaorder1_all seaorder1_all_ind12 seaorder1_ind12 seaorder2_all  seaorder2_all_ind12  seaorder2_ind12 sfqchg_eaorder1 sfqchg_eaorder2 sfqchg_eaorder1_all  sfqchg_eaorder1_all_ind12  sfqchg_eaorder1_ind12 sfqchg_eaorder2_all  sfqchg_eaorder2_all_ind12  sfqchg_eaorder2_ind12  seaorder1_inc  seaorder1_all_inc  sfqchg_eaorder1_inc sfqchg_eaorder1_all_inc seaorder2_inc seaorder2_all_inc   sfqchg_eaorder2_inc  sfqchg_eaorder2_all_inc  seaorder2_ind48  seaorder2_all_ind48  sfqchg_eaorder2_ind48  sfqchg_eaorder2_all_ind48



global census2  i15u2_r i20u2_r i25u2_r i30u2_r  i35u2_r i40u2_r  i50u2_r i75u2_r i125u2_r i200u2_r  i15_150m2_r i15_125m2_r i15_100m2_r i15_75m2_r i15_60m2_r i15_50m2_r i15_45m2_r  i15_40m2_r i20_150m2_r i20_125m2_r i20_100m2_r i20_75m2_r i20_60m2_r i20_50m2_r  i20_45m2_r  i20_40m2_r i25_150m2_r  i25_125m2_r i25_100m2_r i25_75m2_r i25_60m2_r i25_50m2_r i25_45m2_r i30_150m2_r i30_125m2_r i30_100m2_r i30_75m2_r  i30_60m2_r i30_50m2_r i30_45m2_r i35_150m2_r i35_125m2_r i35_100m2_r i35_75m2_r i35_60m2_r i35_50m2_r i40_150m2_r  i40_125m2_r i40_100m2_r i40_75m2_r i40_60m2_r i45_150m2_r i45_125m2_r i45_100m2_r i45_75m2_r i45_60m2_r i50_150m2_r  i50_125m2_r i50_100m2_r i50_75m2_r i60_150m2_r i60_125m2_r i60_100m2_r i60_75m2_r i75_150m2_r i75_125m2_r i75_100m2_r  i100_150m2_r  i100_125m2_r                    d_7_12b_r  d_7_hb_r d_7_gedb_r d_7_scb_r d_7_scb2_r d_7_bb_r d_7_mb_r d_8_12b_r  d_8_hb_r d_8_gedb_r d_8_scb_r d_8_scb2_r d_8_bb_r d_8_mb_r d_9_12b_r  d_9_hb_r d_9_gedb_r d_9_scb_r d_9_scb2_r  d_9_bb_r d_9_mb_r  d_10_12b_r  d_10_hb_r d_10_gedb_r d_10_scb_r  d_10_scb2_r  d_10_bb_r d_10_mb_r                                                                                                        b19013e1 b19025e1_ph b19301e1 b01002e1  b02001e2_r nb02001e2_r b02001e5_r b02001e3_r nb02001e3_r b03003e3_r b03003e2_r i150u1_r  i100u1_r  i60u1_r  i45u1_r  a50u_r  a45u_r  a40u_r  a35u_r  a30u_r  a25u_r  a22u_r  a2560_r  a3060_r  dh_r  dsc_r db_r dm_r  b23025e2_r  work_fh16_r work_f16_r    work_fh25_r  work_f25_r  b19313e1_pp    b19051e2_r   b15012e1_pp     b15012e10_pp b15012e6_pp b15012e_10_6_pp  c15010e1_r  c15010e4_r     i45u2_r i60u2_r i100u2_r i150u2_r  eng_r neng_r b19127e1_pf b19127e1_ph b19127e1_pp b20002e1  b19054e2_r  b19064e1_ph b19064e1_pp b19064e1_pf /**new**/ i45d2_r      a34b_r  a3570_r  



global rp  rp_75_fulltab rp_p1_75_fulltab rp_n1_75_fulltab rp_nu1_75_fulltab rp_p2_75_fulltab rp_n2_75_fulltab rp_nu2_75_fulltab rp_p3_75_fulltab rp_n3_75_fulltab rp_nu3_75_fulltab rp_p4_75_fulltab rp_n4_75_fulltab rp_nu4_75_fulltab rp_100_fulltab rp_p1_100_fulltab rp_n1_100_fulltab rp_nu1_100_fulltab rp_p2_100_fulltab rp_n2_100_fulltab rp_nu2_100_fulltab rp_p3_100_fulltab rp_n3_100_fulltab rp_nu3_100_fulltab rp_p4_100_fulltab rp_n4_100_fulltab rp_nu4_100_fulltab rp_100_pr rp_p1_100_pr  rp_n1_100_pr rp_nu1_100_pr rp_p2_100_pr rp_n2_100_pr rp_nu2_100_pr rp_p3_100_pr rp_n3_100_pr rp_nu3_100_pr rp_p4_100_pr rp_n4_100_pr rp_nu4_100_pr rp_100_pr_nog rp_p1_100_pr_nog rp_n1_100_pr_nog rp_nu1_100_pr_nog rp_p2_100_pr_nog rp_n2_100_pr_nog rp_nu2_100_pr_nog rp_p3_100_pr_nog rp_n3_100_pr_nog rp_nu3_100_pr_nog rp_p4_100_pr_nog rp_n4_100_pr_nog rp_nu4_100_pr_nog rp2_p1_75_fulltab rp2_n1_75_fulltab rp2_nu1_75_fulltab rp2_p2_75_fulltab rp2_n2_75_fulltab rp2_nu2_75_fulltab rp2_p3_75_fulltab rp2_n3_75_fulltab rp2_nu3_75_fulltab rp2_p4_75_fulltab rp2_n4_75_fulltab rp2_nu4_75_fulltab rp3_p1_75_fulltab rp3_n1_75_fulltab rp3_nu1_75_fulltab rp3_p2_75_fulltab rp3_n2_75_fulltab rp3_nu2_75_fulltab rp3_p3_75_fulltab rp3_n3_75_fulltab rp3_nu3_75_fulltab rp3_p4_75_fulltab rp3_n4_75_fulltab rp3_nu4_75_fulltab  rp2_p1_100_fulltab rp2_n1_100_fulltab rp2_nu1_100_fulltab rp2_p2_100_fulltab rp2_n2_100_fulltab rp2_nu2_100_fulltab rp2_p3_100_fulltab rp2_n3_100_fulltab rp2_nu3_100_fulltab rp2_p4_100_fulltab rp2_n4_100_fulltab rp2_nu4_100_fulltab rp3_p1_100_fulltab rp3_n1_100_fulltab rp3_nu1_100_fulltab rp3_p2_100_fulltab rp3_n2_100_fulltab rp3_nu2_100_fulltab rp3_p3_100_fulltab rp3_n3_100_fulltab rp3_nu3_100_fulltab rp3_p4_100_fulltab rp3_n4_100_fulltab rp3_nu4_100_fulltab rp2_p1_100_pr rp2_n1_100_pr rp2_nu1_100_pr rp2_p2_100_pr rp2_n2_100_pr rp2_nu2_100_pr rp2_p3_100_pr rp2_n3_100_pr rp2_nu3_100_pr rp2_p4_100_pr rp2_n4_100_pr rp2_nu4_100_pr rp3_p1_100_pr rp3_n1_100_pr rp3_nu1_100_pr rp3_p2_100_pr rp3_n2_100_pr  rp3_nu2_100_pr rp3_p3_100_pr rp3_n3_100_pr rp3_nu3_100_pr rp3_p4_100_pr rp3_n4_100_pr  rp3_nu4_100_pr rp2_p1_100_pr_nog rp2_n1_100_pr_nog rp2_nu1_100_pr_nog rp2_p2_100_pr_nog rp2_n2_100_pr_nog rp2_nu2_100_pr_nog rp2_p3_100_pr_nog rp2_n3_100_pr_nog rp2_nu3_100_pr_nog rp2_p4_100_pr_nog rp2_n4_100_pr_nog rp2_nu4_100_pr_nog rp3_p1_100_pr_nog rp3_n1_100_pr_nog rp3_nu1_100_pr_nog  rp3_p2_100_pr_nog rp3_n2_100_pr_nog rp3_nu2_100_pr_nog rp3_p3_100_pr_nog rp3_n3_100_pr_nog rp3_nu3_100_pr_nog rp3_p4_100_pr_nog rp3_n4_100_pr_nog  rp3_nu4_100_pr_nog ea_isvi ea_isvi_month ea_isvi_fill ea_isvi_month_fill fqchg_zsc  chg_zsc zsc fqchg_sales chg_sales sales  abs_surp2__16_90 abs_surp2__6_90 abs_surp2__1267_90 abs_surp1 abs_surp2 abs_surp3 abs_surp4 abs_ea_3_ret     sur_er surp_er sue_er   abs_surp2__67_90 abs_surp2_2  ssurp1_er ssurp2_er ssurp3_er sur_chr sue_chr  lag_sur_chr  lag_sue_chr lag4_sur_chr  lag4_sue_chr  sale_sup1 sale_sup6 sale_sup5 sale_sup4 sale_sup6_2 sale_sup5_2 sale_sup1__67_90 sale_sup6__67_90 sale_sup5__67_90  sale_sup4__67_90 sale_sup1__12_90 sale_sup6__12_90 sale_sup5__12_90 sale_sup4__12_90 sale_sup1__16_90 sale_sup6__16_90 sale_sup5__16_90 sale_sup4__16_90 sale_sup1__6_90 sale_sup6__6_90 sale_sup5__6_90 sale_sup4__6_90 sale_sup1__1267_90 sale_sup6__1267_90 sale_sup5__1267_90 sale_sup4__1267_90 b02001e2_r         ea_name_all ea_name_invest ea_name_shop ea_tic_all ea_tic_invest ea_tic_shop ea_g_name_all_fill ea_g_name_invest_fill ea_g_name_shop_fill ea_g_tic_all_fill ea_g_tic_invest_fill ea_g_tic_shop_fill  ea_name_all_1 ea_name_invest_1 ea_name_shop_1 ea_tic_all_1 ea_tic_invest_1 ea_tic_shop_1 ea_g_name_all_fill_1 ea_g_name_invest_fill_1 ea_g_name_shop_fill_1 ea_g_tic_all_fill_1 ea_g_tic_invest_fill_1 ea_g_tic_shop_fill_1   ea_name_all_2 ea_name_invest_2 ea_name_shop_2 ea_tic_all_2 ea_tic_invest_2 ea_tic_shop_2 ea_g_name_all_fill_2 ea_g_name_invest_fill_2 ea_g_name_shop_fill_2 ea_g_tic_all_fill_2 ea_g_tic_invest_fill_2 ea_g_tic_shop_fill_2 



global testind   shrout        log_mve lev  lag_lev  btm  lag_btm  capex_pct  lag_capex_pct  roa  lag_roa  qbhr   size_qbhr  sim_ea   illiquidity  volatil  insti  inside  inside_corr   cov__67_90  disp__67_90  cov__12_90  disp__12_90  cov__16_90  disp__16_90  cov__6_90  disp__6_90  cov__1267_90  disp__1267_90  cov  disp  cov2  disp2   distance_from_home  median_dwell  visits_per_visitor         





winsor2   $base $vars1   surp*    , replace cuts(1 99) 





/**to show robustness to winsorizing daily visit variables at different levels*/
winsor2     visits_by_day   ln_visits_by_day     ,  suffix(_w001)  cuts(0.01 99.99) 
winsor2     visits_by_day   ln_visits_by_day   ,  suffix(_w01)  cuts(0.1 99.9) 
winsor2     visits_by_day   ln_visits_by_day    ,  suffix(_w1)  cuts(1 99) 
winsor2   visits_by_day_norm_cnty_hyb  ln_visits_by_day_norm_cnty_hyb    , replace cuts(1 99) 














cd "/zfs/projects/faculty/suzienoh-gps_proj/CleanData/"


save batch_NSZ_firm_nabs_20211023, replace  












