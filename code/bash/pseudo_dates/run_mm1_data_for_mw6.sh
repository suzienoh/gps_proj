#!/bin/bash

#SBATCH -J mm1_mw6
#SBATCH -p normal
#SBATCH -c 10
#SBATCH --array=1-4
#SBATCH --mem 100G
#SBATCH -t 2879:00
#SBATCH -o ./temp/pseudo-mm1_mw6-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/pseudo_dates/mm1/R_mm1_run_mw6.R ${SLURM_ARRAY_TASK_ID}
Rscript code/Rscript/pseudo_dates/mm1/R_mm1_run_mw6.R ${SLURM_ARRAY_TASK_ID}