#!/bin/bash

#SBATCH -J step2
#SBATCH -p normal
#SBATCH --array=1-6
#SBATCH -c 10
#SBATCH --mem 500G
#SBATCH -t 2879:00
#SBATCH -o ./temp/step2-%j-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

indices='abcdef' # we are running separate jobs for letters a~g
echo $indices


letter=$( echo $indices | cut -c${SLURM_ARRAY_TASK_ID} )
echo We are running index $letter
module load R/4.2.1
echo Rscript code/Rscript/step2_STATA.R $letter
Rscript code/Rscript/step2_STATA.R $letter

## for loop
# expr length $indices
# echo $length
# for i in $(seq 1 $length)
# do
#     echo $i
#     letter=$( echo $indices | cut -c${i} )
#     echo $letter
# done