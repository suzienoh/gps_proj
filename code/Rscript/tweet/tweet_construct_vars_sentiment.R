################################################################################
## Script      : tweet_construct_vars_sentiment.R
## Name        : Eddie Yu
## Description : This script combines two datasets in data/tweet/ and constructs
##               variables
################################################################################


## Preamble, load necessary packages
rm(list = ls(all = TRUE))
library(data.table)
library(dplyr)
library(stargazer)
library(fixest)
library(car)
library(pastecs)
library(stringr)
library(glue)
library(haven)
library(statar)
library(DescTools)
library(vtable)
require(sentimentr)

setwd("/zfs/projects/faculty/suzienoh-gps_proj/Eddie/gps_proj")

# Set filepaths
dataPath1 = "./data/tweet/firm_ea3d_tweets.csv"
dataPath2 = "./data/tweet/tweet_Christina.csv"
absDataPath = "./data/out/batch_NSZ_firm_abs_20211023_partial.csv"
loughranMcDonaldPath = "./data/Loughran-McDonald_MasterDictionary_1993-2021.csv"

sampleTweetDataPath = "./data/out/earnings_mention_positive_tweets.csv"
outPath = "./data/out/firm_abs_positive_tweet_counts.csv"

################################################################################
################################################################################
# Read Data
################################################################################
################################################################################

df1 <- fread(dataPath1)
df2 <- fread(dataPath2, drop=c("V1", "Likes", "Retweets"))
loMc <- fread(loughranMcDonaldPath)
names(df2) <- tolower(names(df2))

number_of_rows = 100000
# main_df <- fread(absDataPath, nrows = number_of_rows)
main_df <- fread(absDataPath) # UNCOMMENT!

################################################################################
################################################################################
# Clean Data types, variable names
################################################################################
################################################################################
main_df[, `:=` (datadate = as.Date(datadate),
                sg_date  = as.Date(sg_date),
                eadate   = as.Date(eadate))]

# Clean df1, df2: Set variable data types for uniformity
df1[, `:=` (datadate = as.Date(datadate),
            eadate   = as.Date(eadate),
            eadatem3 = as.Date(eadatem3),
            eadatep3 = as.Date(eadatep3),
            username = user__name,
            created_at = as.Date(created_at))]
df1 <- df1[, .(gvkey, permno, tic, created_at, full_text, username, 
               datadate, eadate, eadatem3, eadatep3, handle)]
df2[, `:=` (date = NULL,
            `datetime utc` = as.Date(`datetime utc`),
            datadate = as.Date(datadate),
            eadate   = as.Date(eadate),
            eadatem3 = as.Date(eadatem3),
            eadatep3 = as.Date(eadatep3))]
colnames(df2)[1] <- "created_at"
colnames(df2)[2] <- "full_text"
colnames(df2)[9] <- "handle"
df2 <- df2[, .(gvkey, permno, tic, created_at, full_text, username, 
               datadate, eadate, eadatem3, eadatep3, handle)]

################################################################################
################################################################################
# Correct wrongly recorded df2 rows, and append it to df1
################################################################################
################################################################################
    
    # Correct df2 wrongly recorded data: split correct and wrong rows
    df2[, wrong_rows := as.numeric(is.na(created_at) &
                                     full_text != "Could not find Twitter handle for this firm." &
                                     full_text != "No Original Tweets in EA Window")]
    df2_right_rows = df2[wrong_rows==0,]
    df2_wrong_rows = df2[wrong_rows==1,]
    
    # Correct df2_wrong_rows
    anomaly_rows = c(85, 86)
    df2_wrong_rows_1 = df2_wrong_rows[-anomaly_rows,]
    df2_wrong_rows_2 = df2_wrong_rows[anomaly_rows,]
    extract_date <- function(x, substr="??,", index=2, left=1, right=19) {
      str_sub(unlist(strsplit(x, split=substr, fixed=T))[index], left, right)
    }
    
    
    df2_wrong_rows_1[, created_at := lapply(.SD, \(x) str_sub(x,-19,-1)),
                     .SDcols = c("full_text")]
    df2_wrong_rows_2[, `:=` (created_at = NULL,
                             created_at_correct = extract_date(full_text))]
    setcolorder(df2_wrong_rows_2, c("gvkey", "permno", "tic", "created_at_correct"))
    df2_wrong_rows_2 <- df2_wrong_rows_2[order(gvkey, permno, tic, created_at_correct)]
    colnames(df2_wrong_rows_2)[4] <- "created_at"
    df2_wrong_rows <- rbind(df2_wrong_rows_1, df2_wrong_rows_2)
    df2_wrong_rows[, `:=` (created_at = as.Date(created_at))]
    rm(df2_wrong_rows_1, df2_wrong_rows_2)

    # append df2_wrong_rows, df2_right_rows
    df2 <- rbind(df2_right_rows, df2_wrong_rows)
    df2[, wrong_rows := NULL]
    
    # Combine df1, df2, remove duplicates
    df <- unique(rbind(df1, df2))
    rm(df1, df2, df2_right_rows, df2_wrong_rows)


# # Append df1, df2
# df1 <- df1[!is.na(created_at), .(gvkey, permno, datadate, eadate, eadatem3, eadatep3,
#                                  created_at, full_text, handle)]
# df2 <- df2[, .(gvkey, permno, datadate, eadate, eadatem3, eadatep3, 
#                created_at, full_text,  handle)]
# wrong_rows = df2[is.na(created_at) & 
#                    full_text != "Could not find Twitter handle for this firm." &
#                    full_text != "No Original Tweets in EA Window", 
#                  .(gvkey, datadate, eadate, full_text)]
# fwrite(wrong_rows, "./data/out/recording_errors.csv")
# 
# df <- unique(rbind(df1, df2))

################################################################################
################################################################################
# Construct Positive Sentiment Tweets
################################################################################
################################################################################
    # only sift out valid tweets within eadate-3 ~ eadate+3
    print(nrow(df))
    df <- df[, within_eadate3 := as.numeric(created_at >= eadatem3 & created_at <= eadatep3)]
    df <- df[within_eadate3==1,] # 1 row
    df[, within_eadate3 := NULL]
    print(nrow(df))
    
    # SENTIMENT
    sentiment_score <- function(txt) {
      paragraph = sentiment(txt)
      return(crossprod(paragraph$word_count, paragraph$sentiment)[1,1])
    }
    
    sentiment_scores = c()
    for (txt in df$full_text) {
      print(length(sentiment_scores)+1)
      # print(txt)
      score = sentiment_score(txt)
      # print(score)
      sentiment_scores <- append(sentiment_scores, score)
    }
    df <- cbind(df, sentiment_scores)
    print(df[sentiment>0, .(full_text, sentiment)])
    
    
    # Create variable that counts 2 or more of earnings words in tweet
    earnings_words = c("earnings call", "1q", "2q", "3q", "4q", "q1", "q2", "q3", "q4",
                       "ceo\\b", "cfo\\b", "record", "coverage", "announce", "highlight",
                       "investor call", "news release", "guidance", "per share",
                        "earn", "earnings", "release", "eps\\b", "profit", "income",
                       "revenue", "sales", "results", "quarter", "fiscal", 
                       "press release", "investor", "conference", "stock\\b",
                       "financial", "sec\\b", "10-k", "10k", "10-q", "10q",
                       "8-k", "8k", "annual report", "financial statement",
                       "press release", "report", "cash flow", "ebitda\\b", "ebit\\b", "gaap\\b")
    
    # for (word in earnings_words) {
    #   df[, paste0("contains_", word) := as.numeric(grepl(word, full_text, fixed=TRUE))]
    # }
    for (word in earnings_words) {
      # Use grepl with word boundaries
      pattern = paste0("\\b", word)
      df[, paste0("contains_", word) := as.integer(grepl(pattern, full_text, ignore.case = TRUE))]
    }
    print(summary(df))
    contain_vars <- grep("^contains", names(df), value = TRUE)
    df[, sumWords := rowSums(.SD), .SDcols = contain_vars]
    
    
    fwrite(df[sumWords>1 & sentiment > 0, .(gvkey, handle, eadate, full_text, sumWords)],
           sampleTweetDataPath)
    
    df <- df[, !..contain_vars]  
    df[, morethan2earnings_words_pos_sentiment := as.numeric(sumWords>=2 & sentiment>0)]
    
    # remove full_text (tweet) column
    df[, full_text:=NULL]

# is tweet
# df[, `:=` (is_tweet = as.numeric(!is.na(created_at)))]

# collapse down to gvkey X eadate X created_at level
df <- df[, .(num_tweets_per_created_date = .N,
             num_morethan2words_tweet_per_created_date = sum(morethan2words))
         ,by=.(gvkey, permno, tic, handle, 
               created_at, datadate, eadate, eadatem3, eadatep3)]

# collapse down to gvkey X eadate level count
df <- df[, .(num_tweets_around_eadate = sum(num_tweets_per_created_date),
             num_morethan2word_tweets_around_eadate = sum(num_morethan2words_tweet_per_created_date))
         ,by=.(gvkey, permno, tic, handle, datadate, eadate)]

# merge to original dataset
final <- merge(main_df, df, by=c("gvkey", "permno", 'datadate', "eadate"),
               all.x=T)
print("Merge Complete!")
print(summary(final))
rm(main_df, df)

# Create 4 variables:
#  (1) indicator whether there was any tweet during three days around eadate
#  (2) count of total tweets during three days around eadate
#  (3) indicator whether there was any tweet "with two/more earnings words"
#       during three days around eadate
#  (4) count of total tweets "with two/more earnings words"
#       during three days around eadate
final[is.na(num_tweets_around_eadate), num_tweets_around_eadate := 0]
final[is.na(num_morethan2word_tweets_around_eadate), 
      num_morethan2word_tweets_around_eadate := 0]

final[, `:=` (is_tweet_around_eadate = 
                as.numeric(num_tweets_around_eadate > 0),
              is_morethan2word_tweets_around_eadate = 
                as.numeric(num_morethan2word_tweets_around_eadate > 0)
)]

# Winsorize above 4 variables
inter_vars = c("num_tweets_around_eadate",
               "num_morethan2word_tweets_around_eadate",
               "is_tweet_around_eadate",
               "is_morethan2word_tweets_around_eadate")
inter_vars_w = paste0(inter_vars, "_w")
print("Before Winsorizing..")
print(summary(final[, ..inter_vars]))
final[, (inter_vars_w) := lapply(.SD, \(x) Winsorize(x,
                                                     probs=c(0.01, 0.99),
                                                     na.rm=T)),
      .SDcols=(inter_vars)]
print("After Winsorizing..")
print(summary(final[, ..inter_vars_w]))

# Create ln(1+count) for (2), (4)
log_vars = c("num_tweets_around_eadate_w",
             "num_morethan2word_tweets_around_eadate_w")
inter_vars_w = paste0(inter_vars, "_w")
final[, paste0("ln_", log_vars) := lapply(.SD, \(x) log(1+x)),
      .SDcols = (log_vars)]

# filter data
final <- final[d_parent_ticker == 0,]
final <- final[between(aggday, -7, 6), ]


# Write to datapath
fwrite(final, outPath)

quit(save="no")
