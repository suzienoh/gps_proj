#!/bin/bash

#SBATCH -J eatwt
#SBATCH -p normal
#SBATCH --array=1-12
#SBATCH -c 10
#SBATCH --mem 65G
#SBATCH -t 2879:00
#SBATCH -o ./temp/tweet/reg-ea-1word-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/tweet/reg_ea1word_tweet.R ${SLURM_ARRAY_TASK_ID}
Rscript code/Rscript/tweet/reg_ea1word_tweet.R ${SLURM_ARRAY_TASK_ID}