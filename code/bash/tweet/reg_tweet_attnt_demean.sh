#!/bin/bash

#SBATCH -J r_att_dm
#SBATCH -p normal
#SBATCH --array=1-36
#SBATCH -c 3
#SBATCH --mem 90G
#SBATCH -t 2879:00
#SBATCH -o ./temp/tweet/reg-tweet-attnt-demean-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu


module load R/4.2.1
echo Rscript code/Rscript/tweet/reg_tweet_attnt_demean.R ${SLURM_ARRAY_TASK_ID}
Rscript code/Rscript/tweet/reg_tweet_attnt_demean.R ${SLURM_ARRAY_TASK_ID}