#!/bin/bash

#SBATCH -J twt
#SBATCH -p normal
#SBATCH --array=1-4
#SBATCH -c 10
#SBATCH --mem 200G
#SBATCH -t 2879:00
#SBATCH -o ./temp/tweet/reg-ea-tweet-likes1000-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu


module load R/4.2.1
echo Rscript code/Rscript/tweet/reg_ea_tweet_with_likes_1000.R ${SLURM_ARRAY_TASK_ID}
Rscript code/Rscript/tweet/reg_ea_tweet_with_likes_1000.R ${SLURM_ARRAY_TASK_ID}