#!/bin/bash

#SBATCH -J IR-reg
#SBATCH -p normal
#SBATCH --array=1-2
#SBATCH -c 20
#SBATCH --mem 100G
#SBATCH -t 2879:00
#SBATCH -o ./temp/reg-IR.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
if [ ${SLURM_ARRAY_TASK_ID} == 1 ]
then
echo Rscript code/Rscript/IR/run_IR_dropinf_reg.R
Rscript code/Rscript/IR/run_IR_dropinf_reg.R
echo $bool

elif [ ${SLURM_ARRAY_TASK_ID} == 2 ]
then
echo Rscript code/Rscript/IR/run_IR_keepinf_reg.R
Rscript code/Rscript/IR/run_IR_keepinf_reg.R
fi