#!/bin/bash

module load R/4.2.1
dataIndex=1 # array=1-17 for dataIndex=1 & dataIndex=2
for partition in $(seq 1 1 17)
do
    echo Rscript code/Rscript/census/filter_census_data.R $dataIndex $partition
    Rscript code/Rscript/census/filter_census_data.R $dataIndex $partition
done

# read in header
head -1 data/out/census/R_batch_NSZ_census1_20211023_partial_1.csv > data/out/census/R_batch_NSZ_census1_20211023_all_partial_all.csv

# read in data w/o header
for filename in $(ls data/out/census/R_batch_NSZ_census1_20211023_partial_*.csv)
do 
    wc -l $filename
    sed 1d $filename >> data/out/census/R_batch_NSZ_census1_20211023_all_partial_all.csv
done