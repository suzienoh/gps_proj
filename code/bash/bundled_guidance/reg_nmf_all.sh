#!/bin/bash

#SBATCH -J nmf_all
#SBATCH -p normal
#SBATCH --array=1-6
#SBATCH -c 20
#SBATCH --mem 120G
#SBATCH -t 2879:00
#SBATCH -o ./temp/bundled_guidance/nmf_all-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

## run reg_nmf_all.R with winsorization ("T") and taking log(1+x) for arrays=1-3
module load R/4.2.1
split_array=3 ## HALF OF THE SLURM_ARRAY_TASK_ID/2

if [ ${SLURM_ARRAY_TASK_ID} -le ${split_array} ]
then
bool='T'
echo Rscript code/Rscript/bundled_guidance/reg_nmf_all.R ${SLURM_ARRAY_TASK_ID} $bool
Rscript code/Rscript/bundled_guidance/reg_nmf_all.R ${SLURM_ARRAY_TASK_ID} $bool

## run reg_nmf_all.R with no winsorization ("F") for arrays=4-6
elif [ ${SLURM_ARRAY_TASK_ID} -gt ${split_array} ]
then
bool='F'
NEW_TASK_ID=$((${SLURM_ARRAY_TASK_ID} - ${split_array}))
echo Rscript code/Rscript/bundled_guidance/reg_nmf_all.R ${NEW_TASK_ID} $bool
Rscript code/Rscript/bundled_guidance/reg_nmf_all.R ${NEW_TASK_ID} $bool
fi