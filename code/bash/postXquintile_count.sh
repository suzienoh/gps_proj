#!/bin/bash

#SBATCH -J Xquint
#SBATCH -p normal
#SBATCH --array=1-6
#SBATCH -c 10
#SBATCH --mem 80G
#SBATCH -t 2879:00
#SBATCH -o ./temp/count/Xquint-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/count/postXquintile_count.R ${SLURM_ARRAY_TASK_ID}
Rscript code/Rscript/count/postXquintile_count.R ${SLURM_ARRAY_TASK_ID}