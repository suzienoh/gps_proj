#!/bin/bash

#SBATCH -J core_poi
#SBATCH -p normal
#SBATCH -c 10
#SBATCH --mem 300G
#SBATCH -t 2879:00
#SBATCH -o ./temp/clean_core_poi_patterns.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

# Load software
loaded modules
source deactivate
module purge
conda deactivate

module load R/4.2.1
echo Rscript code/Rscript/util/clean_core_poi_patterns.R
Rscript code/Rscript/util/clean_core_poi_patterns.R