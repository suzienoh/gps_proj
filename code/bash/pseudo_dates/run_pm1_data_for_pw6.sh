#!/bin/bash

#SBATCH -J pm1_pw6
#SBATCH -p normal
#SBATCH -c 10
#SBATCH --array=1-3
#SBATCH --mem 120G
#SBATCH -t 2879:00
#SBATCH -o ./temp/pseudo-pm1_pw6-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/pseudo_dates/pm1/R_pm1_run_pw6.R ${SLURM_ARRAY_TASK_ID}
Rscript code/Rscript/pseudo_dates/pm1/R_pm1_run_pw6.R ${SLURM_ARRAY_TASK_ID}