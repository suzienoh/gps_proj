################################################################################
## Script      : bundled_guidance_reg_no_winsor_g0.R
## Name        : Eddie Yu
## Description : This script regresses ln_visits on post * non winsorized g0 
##              (greater than 0) indicator of nmf_cur_eps_3, nmf_sal_3
################################################################################


## Preamble, load necessary packages
rm(list = ls(all = TRUE))
library(data.table)
library(dplyr)
library(stargazer)
library(fixest)
library(car)
library(pastecs)
library(glue)
library(haven)
library(stringr)
library(statar)
library(DescTools)


## Preamble, set wd and load necessary packages
setwd('/zfs/projects/faculty/suzienoh-gps_proj/Eddie/gps_proj')
dataPath <- "./data/out/bundled_guidance_w.csv"

# command line arguments for parallel processing 96 different count variables
args <- commandArgs(trailingOnly = TRUE)
varIndex <- as.numeric(args[1])
# varIndex <- 2 # UNCOMMENT! # 1-8

number_of_rows = 1000000
# df <- fread(dataPath, nrows=number_of_rows) # UNCOMMENT!
df <- fread(dataPath)

df[, `:=` (datadate = as.Date(datadate),
           sg_date  = as.Date(sg_date),
           eadate   = as.Date(eadate))]

nmf_others = grep("^nmf_all_|^nmf_er_", names(df), value=T)
X_vars = c(c("nmf_cur_eps_3", "nmf_sal_3"), nmf_others)
X_var = X_vars[varIndex]

df[, paste0(X_var, "_g0") := lapply(.SD, \(x) as.numeric(x > 0)),
   .SDcols = (X_var)]

# No Winsorization!
# X_var_w = paste0(X_var, "_w")
# df[, (X_var_w) := lapply(.SD, \(x) Winsorize(x,
#                                              probs=c(0.01, 0.99),
#                                              na.rm=T)),
#    .SDcols=(X_var)]

df[, paste0("ln1_", X_var) := lapply(.SD,
                                     \(x) log(1+x)),
   .SDcols=c(X_var)]

# filter data
df <- df[d_parent_ticker == 0,]
df <- df[between(aggday, -7, 6), ]

################################################################################
################################################################################
### REGRESSION
# 2 tables : (1) No winsorized + Just Fourth Qtr (2) No winsor + Post*Fourth Qtr
Xvar_g0 = paste0(X_var, "_g0") 
print(glue("Running for Main Interaction Variable : {Xvar_g0}"))

# (1) No winsorized
outtex = glue("./out/tables/bundled_guidance/{Xvar_g0}.tex")

# (2) No winsorized + Post*fourthqtr_dum
outtex_post4th = glue("./out/tables/bundled_guidance/{Xvar_g0}_post4th.tex")


df <- as.data.frame(df)
clst = c("permno", "ym_eadate_cor") # cluster variables
X_labels = c(fourthqtr_dum="4th Quarter Indicator",
             lag_lossind="Lag Loss Ind", log_mve="Log(MVE)", lag_lev="Lag Lev",
             lag_btm="Lagged Book-to-Market", lag_capex_pct="Lagged Capex \\%", qbhr="Buy-Hold Abn Ret", 
             volatil="Volatility", ln_cov="Analyst Coverage")
setFixest_dict(c(ym_eadate_cor   = "Year-Month FE",
                 place_num       = "Store FE",
                 dow_sg_date     = "Day-of-Week FE",
                 fips_county_hyb = "County FE",
                 permno          = "Firm",
                 naics_code      = "Industry FE"))
setFixest_fml(
  ..interact = ~.[Xvar_g0],
  ..ctrl =  ~lag_lossind + log_mve + lag_lev + lag_btm + lag_capex_pct + fourthqtr_dum + qbhr  + volatil  + ln_cov,
  ..ctrl_post4th = ~lag_lossind + log_mve + lag_lev + lag_btm + lag_capex_pct + post*fourthqtr_dum + qbhr  + volatil  + ln_cov,
  ..fe1 = ~place_num + ym_eadate_cor + dow_sg_date,
  ..fe2 = ~place_num + ym_eadate_cor^naics_code + dow_sg_date,
  ..fe3 = ~place_num + ym_eadate_cor^fips_county_hyb + dow_sg_date,
  ..fe4 = ~place_num + ym_eadate_cor^naics_code + ym_eadate_cor^fips_county_hyb + dow_sg_date,
  ..fe5 = ~place_num + ym_eadate_cor^naics_code + ym_eadate_cor^fips_county_hyb + permno^dow_sg_date,
  ..fe6 = ~place_num + ym_eadate_cor^naics_code + ym_eadate_cor^fips_county_hyb + naics_code^dow_sg_date)
order_cols = c("^post", Xvar_g0, "fourthqtr_dum")
order_cols_post4th = c("^post", Xvar_g0, "fourthqtr_dum")

reg1 <- feols(ln_visits_by_day_w01 ~ post*..interact +  ..ctrl | ..fe1, df, cluster=clst, fixef.rm = "both")
reg1_post4th <- feols(ln_visits_by_day_w01 ~ post*..interact + ..ctrl_post4th | ..fe1, df, cluster=clst, fixef.rm = "both")

## store FE + naics_code*yearmonth FE + dow FE
reg2 <- feols(ln_visits_by_day_w01 ~ post*..interact + fourthqtr_dum + ..ctrl | ..fe2, df, cluster=clst, fixef.rm = "both")
reg2_post4th <- feols(ln_visits_by_day_w01 ~ post*..interact + ..ctrl_post4th | ..fe2, df, cluster=clst, fixef.rm = "both")

## store FE + county*yearmonth FE + dow FE
reg3 <- feols(ln_visits_by_day_w01 ~ post*..interact + fourthqtr_dum + ..ctrl | ..fe3, df, cluster=clst, fixef.rm = "both")
reg3_post4th <- feols(ln_visits_by_day_w01 ~ post*..interact + ..ctrl_post4th | ..fe3, df, cluster=clst, fixef.rm = "both")

## store + county*yearmonth + naics_codeS*yearmonth FE + dow FE
reg4 <- feols(ln_visits_by_day_w01 ~ post*..interact + fourthqtr_dum + ..ctrl | ..fe4, df, cluster=clst, fixef.rm = "both")
reg4_post4th <- feols(ln_visits_by_day_w01 ~ post*..interact + ..ctrl_post4th | ..fe4, df, cluster=clst, fixef.rm = "both")

## store + county*yearmonth + naics_codeS*yearmonth FE + firm*dow FE
reg5 <- feols(ln_visits_by_day_w01 ~ post*..interact + fourthqtr_dum + ..ctrl | ..fe5, df, cluster=clst, fixef.rm = "both")
reg5_post4th <- feols(ln_visits_by_day_w01 ~ post*..interact + ..ctrl_post4th | ..fe5, df, cluster=clst, fixef.rm = "both")

## store + county*yearmonth + naics_codeS*yearmonth FE + naics*dow FE
reg6 <- feols(ln_visits_by_day_w01 ~ post*..interact + fourthqtr_dum + ..ctrl | ..fe5, df, cluster=clst, fixef.rm = "both")
reg6_post4th <- feols(ln_visits_by_day_w01 ~ post*..interact + ..ctrl_post4th | ..fe6, df, cluster=clst, fixef.rm = "both")

## output main results
print(glue("Outputting to {outtex}"))
etable( reg1, reg2, reg3, reg4, reg5, reg6,
        # etable( reg4, reg5, reg6,
        depvar=FALSE, headers = paste0(Xvar_g0, ": Not Winsorized"), drop.section="fixef",
        fontsize = "footnotesize",
        file=outtex, replace = TRUE, digits = "r3", digits.stats = "r3",
        coefstat= "tstat",  dict = X_labels, fitstat = ~ r2 + n, order=order_cols)


etable( reg1_post4th, reg2_post4th, reg3_post4th, reg4_post4th, reg5_post4th, reg6_post4th,
        # etable( reg4_post4th, reg5_post4th, reg6_post4th,
        depvar=FALSE, headers = paste0(Xvar_g0, ": Not Winsorized, (WITH Post $\\times$ 4th Qtr)"), drop.section="fixef",
        fontsize = "footnotesize",
        file=outtex_post4th, replace = TRUE, digits = "r3", digits.stats = "r3",
        coefstat= "tstat",  dict = X_labels, fitstat = ~ r2 + n, order=order_cols)

# rm(reg1, reg2, reg3, reg4, reg5, reg6,
#    reg1_post4th, reg2_post4th, reg3_post4th, reg4_post4th, reg5_post4th, reg6_post4th)

quit(save="no")