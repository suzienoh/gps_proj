################################################################################
## Script      : run_IR_keepinf_reg.R
## Name        : Eddie Yu
## Description : This script runs different triple interaction specifications
################################################################################


## Preamble, load necessary packages
rm(list = ls(all = TRUE))
library(data.table)
library(dplyr)
library(stargazer)
library(fixest)
library(car)
library(pastecs)
library(glue)
library(haven)
library(stringr)
library(statar)

## Set working directory
setwd("/zfs/projects/faculty/suzienoh-gps_proj/Eddie/gps_proj")

# specify filepath
# command line arguments for parallel processing two regressions
args <- commandArgs(trailingOnly = TRUE)
ntile <- args[1]=="T"
if (ntile == TRUE) {
  print("Fetching ntile data..")
  dataPath = "./data/out/IR_keepinf_eadate_t0_ntile_post_IR_f5ym_surp6_2.csv"
  outtex = "./out/tables/IR/IR_keepinf_eadate_t0_ntile_post_IR_f5ym_surp6_2.tex"
  title = "ntile"
} else if (ntile == FALSE) {
  print("Fetching xtile data..")
  dataPath = "./data/out/IR_keepinf_eadate_t0_xtile_post_IR_f5ym_surp6_2.csv"
  outtex = "./out/tables/IR/IR_keepinf_eadate_t0_xtile_post_IR_f5ym_surp6_2.tex"
  title = "xtile"
} else {
  stop("ValueError: Input T/F value for ntile")
}



# read input file
print(glue("Reading Data FilePath: {dataPath}"))
number_rows = 500000
# df <- fread(dataPath, nrows = number_rows) # UNCOMMENT LATER
df <- fread(dataPath)

df[, `:=` (gvkey = as.factor(gvkey),
           naics_code = as.factor(naics_code),
           nai2 = as.factor(nai2),
           fips_county_hyb = as.factor(fips_county_hyb),
           eadate = as.Date(eadate, format="%d%b%Y"),
           sg_date = as.Date(sg_date, format="%d%b%Y"),
           datadate = as.Date(datadate, format="%d%b%Y"))] 

# compute median surp
df[, `:=` (med_surp6_2 = median(surp6_2, na.rm=TRUE)), by=.(ym_eadate_cor)]
df[, `:=` (hfym_surp6_2 = as.numeric(surp6_2 > med_surp6_2))]


if (nrow(df[is.na(surp6_2),])!=0 | nrow(df[!is.na(ym_surp6_2),])==0) {
  stop("ValueError: There is NA value in dataframe")
}


# indicator
num_bins     = 5
quintile_var = "ym_surp6_2"
for (i in 1:num_bins) {
  df[, paste0("f", i, quintile_var) := ifelse(get(quintile_var) == i, 
                                              1,
                                              0)]
}

df[, f234ym_surp6_2 := as.numeric(
  (f2ym_surp6_2==1) | (f3ym_surp6_2==1) | (f4ym_surp6_2==1)
)]

# interactions
interact_vars = c("f1ym_surp6_2", "f5ym_surp6_2", "f234ym_surp6_2", "hfym_surp6_2")
df[, paste0("postIR_X_", interact_vars) := lapply(.SD, function(x) post * IR * x),
   .SDcols = interact_vars]
df[, paste0("post_X_", interact_vars) := lapply(.SD, function(x) post * x),
   .SDcols = interact_vars]
df[, paste0("IR_X_", interact_vars) := lapply(.SD, function(x) IR * x),
   .SDcols = interact_vars]
df[, post_IR := post * IR]


# regressions
df = as.data.frame(df)
print(glue("Total number of rows: {nrow(df)}"))
clst = c("permno", "ym_eadate_cor") # cluster variables
X_labels = c(postIR_X_f5ym_surp6_2="Post $\\times$ IR $\\times$ Q5 SURP",
             IR_X_f5ym_surp6_2="IR $\\times$ Q5 SURP",
             post_X_f5ym_surp6_2="Post $\\times$ Q5 SURP",
             post_IR="Post $\\times$ IR",
             post="Post", IR="IR", f5ym_surp6_2="Q5 SURP",
             lag_lossind="Lag Loss Ind", log_mve="Log(MVE)", lag_lev="Lag Lev",
             lag_btm="Lagged Book-to-Market", lag_capex_pct="Lagged Capex \\%", qbhr="Buy-Hold Abn Ret", 
             volatil="Volatility", ln_cov="Analyst Coverage",
             postIR_X_f1ym_surp6_2="Post $\\times$ IR $\\times$ Q1 SURP",
             post_X_f1ym_surp6_2="Post $\\times$ Q1 SURP", IR_X_f1ym_surp6_2="IR $\\times$ Q1 SURP", 
             f1ym_surp6_2="Q1 SURP", postIR_X_f234ym_surp6_2="Post $\\times$ IR $\\times$ Q2-Q4 SURP", 
             post_X_f234ym_surp6_2="Post $\\times$ Q2-Q4 SURP", IR_X_f234ym_surp6_2="IR $\\times$ Q2-Q4 SURP")



setFixest_fml(
  # ..interact = ~postIR_X_f5ym_surp6_2 + post_X_f5ym_surp6_2 + IR_X_f5ym_surp6_2 + 
  # postIR_X_f1ym_surp6_2 + post_X_f1ym_surp6_2 + IR_X_f1ym_surp6_2 + f5ym_surp6_2 + f1ym_surp6_2 + post_IR + post + IR,
  ..interact = ~post*IR*f5ym_surp6_2,
  ..ctrl = ~lag_lossind + log_mve + lag_lev + lag_btm + lag_capex_pct + fourthqtr_dum  + qbhr  + volatil  + ln_cov,
  ..fe1 = ~place_num + ym_eadate_cor + dow_sg_date,
  ..fe2 = ~place_num + ym_eadate_cor^naics_code + dow_sg_date,
  ..fe3 = ~place_num + ym_eadate_cor^fips_county_hyb + dow_sg_date,
  ..fe4 = ~place_num + ym_eadate_cor^naics_code + ym_eadate_cor^fips_county_hyb + dow_sg_date,
  ..fe5 = ~place_num + ym_eadate_cor^naics_code + ym_eadate_cor^fips_county_hyb + permno^dow_sg_date,
  ..fe6 = ~place_num + ym_eadate_cor^naics_code + ym_eadate_cor^fips_county_hyb + naics_code^dow_sg_date)
# order_cols = c("^postIR_", "post_X_f5ym_surp6_2", "^IR_", "post_IR", "IR", "post", "^f5ym_")
order_cols = c("^post", "^IR", "^f5ym")

reg1 <- feols(ln_visits_by_day_w01 ~  ..interact + ..ctrl | ..fe1, df, cluster=clst, fixef.rm = "both")
reg2 <- feols(ln_visits_by_day_w01 ~  ..interact + ..ctrl | ..fe2, df, cluster=clst, fixef.rm = "both")
reg3 <- feols(ln_visits_by_day_w01 ~  ..interact + ..ctrl | ..fe3, df, cluster=clst, fixef.rm = "both")
reg4 <- feols(ln_visits_by_day_w01 ~  ..interact + ..ctrl | ..fe4, df, cluster=clst, fixef.rm = "both")
reg5 <- feols(ln_visits_by_day_w01 ~  ..interact + ..ctrl | ..fe5, df, cluster=clst, fixef.rm = "both")
reg6 <- feols(ln_visits_by_day_w01 ~  ..interact + ..ctrl | ..fe6, df, cluster=clst, fixef.rm = "both")

etable(reg1, reg2, reg3, reg4, reg5, reg6,
       file=outtex, replace = TRUE, digits = "r3", digits.stats = "r3",
       headers = paste0("Quintile Function: ", title),
       coefstat= "tstat", fitstat = ~ r2 + n, dict = X_labels, order=order_cols)

## Produce Summary Statistics
desc_vars = c("IR")

case.names.fixest <- function(object, ...){
  no <- object$obs_selection$obsRemoved
  return(seq_len(object$nobs_origin)[no])
}

if (ntile == T) {
  esample_rowid = case.names(reg1) # e(sample) from reg1
  df = setDT(df)
  esample = df[esample_rowid,]
  
  sumtable(esample[, ..desc_vars],
           add.median=T,
           summ.names=c('N','Mean','S.D', 'Min', 'Q1', 'Q2', 'Q3', 'Max'),
           out="latex",
           fit.page = '0.6\\textwidth',
           file=glue("./out/tables/IR/desc_stats_IR.tex"))
  collapse_vars = c(c("gvkey", "datadate"), desc_vars)
  esample_collapsed =
    unique(as.data.table(esample[, ..collapse_vars] %>%
                           group_by(gvkey, datadate)))
  sumtable(esample_collapsed[, ..desc_vars],
           add.median=T,
           summ.names=c('N','Mean','S.D', 'Min', 'Q1', 'Q2', 'Q3', 'Max'),
           out="latex",
           title='Summary Statistics - Collapsed to Firm-Quarter level',
           fit.page = '0.6\\textwidth',
           file=glue("./out/tables/IR/desc_stats_IR_collapsed.tex"))
  rm(reg1)
}




quit(save="no")