#!/bin/bash

#SBATCH -J census1
#SBATCH -p normal
#SBATCH -c 5
#SBATCH --array=3
#SBATCH --mem 230G
#SBATCH -t 200:00
#SBATCH -o ./temp/census/run-census1-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu


module load R/4.2.1
echo Rscript code/Rscript/census/demographic_census1.R ${SLURM_ARRAY_TASK_ID} 
Rscript code/Rscript/census/demographic_census1.R ${SLURM_ARRAY_TASK_ID}