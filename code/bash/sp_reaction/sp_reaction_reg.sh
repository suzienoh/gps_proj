#!/bin/bash

#SBATCH -J sp_reg
#SBATCH -p normal
#SBATCH --array=1-28
#SBATCH -c 10
#SBATCH --mem 75G
#SBATCH -t 2879:00
#SBATCH -o ./temp/sp_reg-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu


module load R/4.2.1
echo Rscript code/Rscript/sp_reaction/sp_reaction_reg.R ${SLURM_ARRAY_TASK_ID}
Rscript code/Rscript/sp_reaction/sp_reaction_reg.R ${SLURM_ARRAY_TASK_ID}