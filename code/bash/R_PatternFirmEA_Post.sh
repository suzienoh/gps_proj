#!/bin/bash

#SBATCH -J Pattern
#SBATCH -p normal
#SBATCH -c 10
#SBATCH --mem 500G
#SBATCH -t 2879:00
#SBATCH -o ./temp/R_PatternFirmEA_Post.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/apndxD/R_PatternFirmEA_Post.R
Rscript code/Rscript/apndxD/R_PatternFirmEA_Post.R