#!/bin/bash

#SBATCH -J Xcnt
#SBATCH -p normal
#SBATCH --array=1-6
#SBATCH -c 10
#SBATCH --mem 50G
#SBATCH -t 2879:00
#SBATCH -o ./temp/count/count-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/count/postXcount.R ${SLURM_ARRAY_TASK_ID}
Rscript code/Rscript/count/postXcount.R ${SLURM_ARRAY_TASK_ID}