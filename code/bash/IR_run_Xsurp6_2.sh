#!/bin/bash

#SBATCH -J Xsurp2
#SBATCH -p normal
#SBATCH --array=1-24
#SBATCH -c 10
#SBATCH --mem 100G
#SBATCH -t 2879:00
#SBATCH -o ./temp/Xsurp-%j-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/IR/IR_triple_interaction.R ${SLURM_ARRAY_TASK_ID}
Rscript code/Rscript/IR/IR_triple_interaction.R ${SLURM_ARRAY_TASK_ID}