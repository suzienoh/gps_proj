#!/bin/bash

#SBATCH -J se_naic
#SBATCH -p normal
#SBATCH -c 10
#SBATCH --mem 50G
#SBATCH -t 2879:00
#SBATCH -o ./temp/count/se_naic.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/count/postXse_naic.R
Rscript code/Rscript/count/postXse_naic.R