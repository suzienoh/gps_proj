#!/bin/bash

#SBATCH -J prep
#SBATCH -p normal
#SBATCH -c 10
#SBATCH --mem 400G
#SBATCH -t 2879:00
#SBATCH -o ./temp/master-data.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/util/prepare_master_data.R
Rscript code/Rscript/util/prepare_master_data.R