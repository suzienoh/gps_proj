#!/bin/bash

#SBATCH -J bundle
#SBATCH -p normal
#SBATCH --array=1-72
#SBATCH -c 10
#SBATCH --mem 80G
#SBATCH -t 2879:00
#SBATCH -o ./temp/bundled_guidance/bundle-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/bundled_guidance/bundled_guidance_reg.R ${SLURM_ARRAY_TASK_ID}
Rscript code/Rscript/bundled_guidance/bundled_guidance_reg.R ${SLURM_ARRAY_TASK_ID}