#!/bin/bash

#SBATCH -J R_RP
#SBATCH -p normal
#SBATCH -c 30
#SBATCH --mem 400G
#SBATCH -t 2879:00
#SBATCH -o ./temp/R_RP.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/table3/R_RP_hardcodeFE.R
Rscript code/Rscript/table3/R_RP_hardcodeFE.R