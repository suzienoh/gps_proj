#!/bin/bash

#SBATCH -J dyn_allspecs
#SBATCH -p normal
#SBATCH --array=91-96
#SBATCH -c 4
#SBATCH --mem 70G
#SBATCH -t 180:00
#SBATCH -o ./temp/dyn/dyn-reg-allspecs-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu


task_id=$((${SLURM_ARRAY_TASK_ID} - 1))
dataIndex=$(($task_id / 16 + 1))
remainder=$(($task_id % 16))

echo Dataset Index $dataIndex, Remainder $remainder

module load R/4.2.1
if [ ${remainder} -eq 0 ]
then
subsampling=1
bool=0
daily=0

elif [ ${remainder} -eq 1 ]
then
subsampling=1
bool=0
daily=1

elif [ ${remainder} -eq 2 ]
then
subsampling=1
bool=1
daily=0

elif [ ${remainder} -eq 3 ]
then
subsampling=1
bool=1
daily=1

elif [ ${remainder} -eq 4 ]
then
subsampling=2
bool=0
daily=0

elif [ ${remainder} -eq 5 ]
then
subsampling=2
bool=0
daily=1

elif [ ${remainder} -eq 6 ]
then
subsampling=2
bool=1
daily=0

elif [ ${remainder} -eq 7 ]
then
subsampling=2
bool=1
daily=1

elif [ ${remainder} -eq 8 ]
then
subsampling=3
bool=0
daily=0

elif [ ${remainder} -eq 9 ]
then
subsampling=3
bool=0
daily=1

elif [ ${remainder} -eq 10 ]
then
subsampling=3
bool=1
daily=0

elif [ ${remainder} -eq 11 ]
then
subsampling=3
bool=1
daily=1

elif [ ${remainder} -eq 12 ]
then
subsampling=4
bool=0
daily=0

elif [ ${remainder} -eq 13 ]
then
subsampling=4
bool=0
daily=1

elif [ ${remainder} -eq 14 ]
then
subsampling=4
bool=1
daily=0

elif [ ${remainder} -eq 15 ]
then
subsampling=4
bool=1
daily=1
fi


echo Rscript code/Rscript/dyn_reg/dyn_reg_allspecs.R $dataIndex $subsampling $bool $daily
Rscript code/Rscript/dyn_reg/dyn_reg_allspecs.R $dataIndex $subsampling $bool $daily