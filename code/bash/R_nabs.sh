#!/bin/bash

#SBATCH -J R_nabs
#SBATCH -p normal
#SBATCH -c 10
#SBATCH --mem 300G
#SBATCH -t 2879:00
#SBATCH -o ./temp/R_nabs.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/table3/R_nabs_hardcodeFE.R
Rscript code/Rscript/table3/R_nabs_hardcodeFE.R