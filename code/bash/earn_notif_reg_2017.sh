#!/bin/bash

#SBATCH -J notif17
#SBATCH -p normal
#SBATCH --array=1-3
#SBATCH -c 10
#SBATCH --mem 40G
#SBATCH -t 2879:00
#SBATCH -o ./temp/earn_notif-2017-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/earn_notif/earn_notif_reg_2017.R ${SLURM_ARRAY_TASK_ID}
Rscript code/Rscript/earn_notif/earn_notif_reg_2017.R ${SLURM_ARRAY_TASK_ID}