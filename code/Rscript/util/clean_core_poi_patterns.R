################################################################################
## Script      : clean_core_poi_patterns.R
## Name        : Eddie Yu
## Description : This script processes the following three datasets
################################################################################


## Preamble, load necessary packages
rm(list = ls(all = TRUE))
library(data.table)
library(dplyr)
library(glue)

## Two input filepaths
rootPath = '/zfs/projects/faculty/suzienoh-gps_proj/CleanData/'
setwd(rootPath)
dataPaths = c("./core_poi-patterns_parsed_201701to202002_big.csv",
              "./core_poi-patterns_parsed_201701to202002_small.csv")

# output filepath
outPaths = gsub("\\.csv$", "_out.csv", dataPaths)


# loop through files
for (index in 1:length(dataPaths)) {
  # index = 1 # UNCOMMENT!
  print(glue("Processing File Index {index}"))
  input_path  = dataPaths[index]
  output_path = outPaths[index]
  print(glue("Reading filepath: {input_path} / Writing to filepath {output_path}"))
  
  # read input file
  num_rows = 1000
  df <- fread(input_path)
  # df <- fread(input_path, nrows=num_rows) # UNCOMMENT!
  
  # remove rows with missing safegraph_brand_ids and iso_country_code="CA"
  df <- df[!(is.na(safegraph_brand_ids) & iso_country_code == "CA")]
  
  # keep only the following columns
  keep_cols <- c(
    "Year",
    "Month",
    "safegraph_place_id",
    "location_name",
    "safegraph_brand_ids",
    "brands",
    "naics_code",
    "latitude",
    "longitude",
    "city",
    "region",
    "postal_code",
    "raw_visit_counts",
    "raw_visitor_counts",
    "distance_from_home",
    "median_dwell"
  )
  
  # home_agg_i_group (where i=1 ...200)
  visitor_home_cbgs_group_cols = paste0("visitor_home_cbgs_", c(1:200), "_group") 
  
  # home_agg_i_number (where i=1 ...200)
  visitor_home_cbgs_number_cols = paste0("visitor_home_cbgs_", c(1:200), "_number")
  
  # all of the above columns
  keep_cols = c(keep_cols, visitor_home_cbgs_group_cols, visitor_home_cbgs_number_cols)
  
  # leave only keep_cols in df
  df <- df[, ..keep_cols]
  
  
  ### keep rows with certain brand names ###
  ### ADD / CHANGE AT YOUR CONVENIENCE!  ###
  brand_names = c("sprint", "at & t", "at&t", "t-mobile", "ford", "tesla", "mcdonald", 
                  "shake shack", "shake", "walmart", "costco", "bj")
  
  for (brand_name in brand_names) {
    # Use grepl with word boundaries (ignore)
    # pattern = paste0("\\b", brand_name)
    match_pattern = brand_name
    df[, paste0("contains_", brand_name) := as.integer(grepl(match_pattern, 
                                                             brands, 
                                                             ignore.case = TRUE))]
  }
  contain_vars <- grep("^contains", names(df), value = TRUE) # columns
  print(summary(df[, ..contain_vars]))
  df[, count_brands := rowSums(.SD), .SDcols = contain_vars] # sum all contain_* cols
  df[, contains_any_brands := as.numeric(count_brands > 0)] # indicator for containing brand name
  df <- df[, !..contain_vars]
  
  # keep rows where contain_any_brands = 1
  df <- df[contains_any_brands == 1,]
  
  # Write to outpath 
  fwrite(df, output_path)
  
}


quit(save = "no")

