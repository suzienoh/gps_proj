#!/bin/bash

#SBATCH -J twt
#SBATCH -p normal
#SBATCH --array=1-132
#SBATCH -c 10
#SBATCH --mem 200G
#SBATCH -t 2879:00
#SBATCH -o ./temp/tweet/reg-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu


module load R/4.2.1
echo Rscript code/Rscript/tweet/tweet_reg_sentiment_like_retweets.R ${SLURM_ARRAY_TASK_ID}
Rscript code/Rscript/tweet/tweet_reg_sentiment_like_retweets.R ${SLURM_ARRAY_TASK_ID}