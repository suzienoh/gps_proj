#!/bin/bash

#SBATCH -J tbl
#SBATCH -p normal
#SBATCH --array=1-2
#SBATCH -c 30
#SBATCH --mem 500G
#SBATCH -t 2879:00
#SBATCH -o ./temp/suzie-table-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu


module load R/4.2.1
echo Rscript code/Rscript/IR/suzie_table.R ${SLURM_ARRAY_TASK_ID}
Rscript code/Rscript/IR/suzie_table.R ${SLURM_ARRAY_TASK_ID}
