#!/bin/bash

#SBATCH -J eatwt
#SBATCH -p normal
#SBATCH --array=1-2
#SBATCH -c 10
#SBATCH --mem 250G
#SBATCH -t 2879:00
#SBATCH -o ./temp/ea-twt-construct-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu


module load R/4.2.1
echo Rscript code/Rscript/tweet/ea1word_tweet_construct_vars.R ${SLURM_ARRAY_TASK_ID}
Rscript code/Rscript/tweet/ea1word_tweet_construct_vars.R ${SLURM_ARRAY_TASK_ID}