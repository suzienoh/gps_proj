#!/bin/bash

#SBATCH -J crosstest
#SBATCH -p normal
#SBATCH --array=1-3
#SBATCH -c 20
#SBATCH --mem 400G
#SBATCH -t 2879:00
#SBATCH -o ./temp/cross-test-%j-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/eadate_eanotif_IR_cross_sectional.R ${SLURM_ARRAY_TASK_ID}
Rscript code/Rscript/eadate_eanotif_IR_cross_sectional.R ${SLURM_ARRAY_TASK_ID}