#!/bin/bash

#SBATCH -J bundle
#SBATCH -p normal
#SBATCH --array=3-8
#SBATCH -c 10
#SBATCH --mem 90G
#SBATCH -t 2879:00
#SBATCH -o ./temp/bundled_guidance/bundle-no-w-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/bundled_guidance/bundled_guidance_reg_no_winsor.R ${SLURM_ARRAY_TASK_ID}
Rscript code/Rscript/bundled_guidance/bundled_guidance_reg_no_winsor.R ${SLURM_ARRAY_TASK_ID}

# echo Rscript code/Rscript/bundled_guidance/bundled_guidance_reg_g0.R ${SLURM_ARRAY_TASK_ID}
# Rscript code/Rscript/bundled_guidance/bundled_guidance_reg_g0.R ${SLURM_ARRAY_TASK_ID}