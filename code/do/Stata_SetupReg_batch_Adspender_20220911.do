

	   
	   
	   
clear all
set maxvar   32767, permanently
set more off, permanently



use "/zfs/projects/faculty/suzienoh-gps_proj/CleanData/NSZ_firm_abs_Adspender.dta", clear   





rename *, lower 


gen yr_eadate = year(eadate)
gen qtr_eadate =quarter(eadate)
gen m_eadate = month(eadate)
gen yq_eadate = yr_eadate*10 + qtr_eadate
gen ym_eadate = yr_eadate*10 + m_eadate




gen double yqea_ind48 = yq_eadate*1000 + ff_48  /*for year-quarter X industry fixed effects*/
gen double ymea_ind48 = ym_eadate*1000 + ff_48  /*for year-quarter X industry fixed effects*/


gen double yqea_naic = yq_eadate*10000000 + naic  /*for year-quarter X industry fixed effects*/
gen double ymea_naic = ym_eadate*10000000 + naic  /*for year-quarter X industry fixed effects*/


gen double permno_yqact = yq_eadate*10000000  + permno /*for year-quarter X firm fixed effects*/
gen double permno_ymact = ym_eadate*10000000  + permno /*for year-quarter X firm fixed effects*/


egen place_num = group(safegraph_place_id)



gen double place_yqact = yq_eadate*100000000  + place_num /*for year-quarter X firm fixed effects*/
gen double place_ymact = ym_eadate*100000000  + place_num /*for year-quarter X firm fixed effects*/


 

egen region_num = group(region)

tabulate region
tabulate region_num, nolabel


gen double region_yqact = yq_eadate*1000  + region_num /*for year-quarter X region fixed effects*/
gen double region_ymact = ym_eadate*1000  + region_num /*for year-quarter X region fixed effects*/



egen city_num = group(city)


gen double city_yqact = yq_eadate*100000000  + city_num /*for year-quarter X region fixed effects*/
gen double city_ymact = ym_eadate*100000000  + city_num /*for year-quarter X region fixed effects*/



gen double fips_c_h_yqact = yq_eadate*100000000  + fips_county_hyb /*for year-quarter X region fixed effects*/
gen double fips_c_h_ymact = ym_eadate*100000000  + fips_county_hyb /*for year-quarter X region fixed effects*/

	


format yqea_ind48  ymea_ind48  yqea_naic  ymea_naic  permno_yqact  permno_ymact  place_yqact   place_ymact  region_yqact  region_ymact  city_yqact  city_ymact  fips_c_h_yqact  fips_c_h_ymact   %15.0g




cd "/zfs/projects/faculty/suzienoh-gps_proj/CleanData/"






 
 






local ccgrp "visits_by_day_norm_cnty_hyb  visits_by_day " 
local n : word count `ccgrp'
forvalues i = 1/`n' {
local a : word `i' of `ccgrp'

gen ln_`a'=ln(1+`a')

}







replace cov=0  if missing(cov)
replace cov2=0  if missing(cov2)
 



local ccgrp "__67_90  __12_90   __16_90 __6_90  __1267_90" 
local n : word count `ccgrp'
forvalues i = 1/`n' {
local a : word `i' of `ccgrp'

replace cov`a'=0  if missing(cov`a')


gen ln_cov`a'=ln(1+cov`a')

}



gen ln_cov=ln(1+cov)
gen ln_cov2=ln(1+cov2)



gen all =1




 
 
 


****drop subsidiaries****	
drop if d_parent_ticker==1















global base /*shrout*/ qbhr  size_qbhr sim_ea illiquidity volatil insti inside inside_corr /*prccq*/ log_mve /*lossind */ lev btm capex_pct roa /*lag_lossind */ lag_lev lag_btm lag_capex_pct lag_roa /*fourthqtr_dum*/ /*friday*/  /*sg_date_friday*/  cov cov2 ln_cov__67_90  ln_cov__12_90  ln_cov__16_90  ln_cov__6_90 ln_cov__1267_90 ln_cov  ln_cov2  disp__67_90  disp__12_90  disp__16_90 disp__6_90  disp__1267_90   disp disp2 distance_from_home   median_dwell visits_per_visitor ufe ufe2 ufe__67_90  ufe__12_90   ufe__16_90 ufe__6_90  ufe__1267_90  medest medest2 medest__67_90  medest__12_90   medest__16_90 medest__6_90  medest__1267_90 actual actual2 actual__67_90  actual__12_90   actual__16_90 actual__6_90  actual__1267_90


global vars1     totaldols000 totalunits networktvdols000  networktvunits cabletvdols000  cabletvunits  syndicationdols000 syndicationunits spottvdols000 spottvunits magazinesdols000 magazinesunits sundaymagsdols000 sundaymagsunits natlnewspdols000 natlnewspunits newspaperdols000 newspaperunits networkradiodols000 networkradiounits natspotradiodols000 natspotradiounits intdisplaydols000 intdisplayunits outdoordols000 outdoorunits totaldols000_f totalunits_f networktvdols000_f networktvunits_f cabletvdols000_f cabletvunits_f syndicationdols000_f syndicationunits_f spottvdols000_f spottvunits_f magazinesdols000_f magazinesunits_f sundaymagsdols000_f sundaymagsunits_f natlnewspdols000_f natlnewspunits_f newspaperdols000_f newspaperunits_f networkradiodols000_f networkradiounits_f natspotradiodols000_f natspotradiounits_f intdisplaydols000_f intdisplayunits_f																					outdoordols000_f outdoorunits_f




winsor2   $base $vars1   , replace cuts(1 99) 





winsor2     visits_by_day   ln_visits_by_day     ,  suffix(_w001)  cuts(0.01 99.99) 
winsor2     visits_by_day   ln_visits_by_day   ,  suffix(_w01)  cuts(0.1 99.9) 
winsor2     visits_by_day   ln_visits_by_day    ,  suffix(_w1)  cuts(1 99) 
winsor2   visits_by_day_norm_cnty_hyb  ln_visits_by_day_norm_cnty_hyb    , replace cuts(1 99) 














cd "/zfs/projects/faculty/suzienoh-gps_proj/CleanData/"




 
 
local ccgrp " totaldols000 totalunits networktvdols000  networktvunits cabletvdols000  cabletvunits  syndicationdols000 syndicationunits spottvdols000 spottvunits magazinesdols000 magazinesunits sundaymagsdols000 sundaymagsunits natlnewspdols000 natlnewspunits newspaperdols000 newspaperunits networkradiodols000 networkradiounits natspotradiodols000 natspotradiounits intdisplaydols000 intdisplayunits outdoordols000 outdoorunits totaldols000_f totalunits_f networktvdols000_f networktvunits_f cabletvdols000_f cabletvunits_f syndicationdols000_f syndicationunits_f spottvdols000_f spottvunits_f magazinesdols000_f magazinesunits_f sundaymagsdols000_f sundaymagsunits_f natlnewspdols000_f natlnewspunits_f newspaperdols000_f newspaperunits_f networkradiodols000_f networkradiounits_f natspotradiodols000_f natspotradiounits_f intdisplaydols000_f intdisplayunits_f																					outdoordols000_f outdoorunits_f" 
local n : word count `ccgrp'
forvalues i = 1/`n' {
local a : word `i' of `ccgrp'

gen ln1_`a'=ln(1+`a')
gen ln_`a'=ln(`a')
}





***************************************************************************
********creating "aggday" to capture days relative to the event date*******
********and keeping only 10 days before and after the event date***********
***************************************************************************

sort permno place_num eadate post sg_date  
by permno place_num eadate post: generate new_n = _n  /*first observation in pre starts from 1.*/  
replace new_n = new_n -1 if post==1 /*first observation in post starts from 0.*/ 



bysort permno place_num eadate post: egen post_max_new_n = max(new_n)


gen aggday = ((-1)*(post_max_new_n) + (new_n - 1)) if post==0

replace aggday = new_n if post==1

drop if aggday >= 11 
drop if aggday <= -11



keep if d_parent_ticker == 0





*****************************************************************
***************creating variables needed for regressions*********
*****************************************************************


**day of week variables and fixed effects**
gen dow_sg_date = dow(sg_date)
tabulate  dow_sg_date, gen(dow_fe)


**firm-datatdate(fiscal quarter end) fixed effects**
gen double permno_datadate = datadate*10000000  + permno 
format permno_datadate   %15.0g

   



**creating an interactive term to be used later in regressions**

gen  Xvar_surp = .
	   
	   
	 
	   
*****************************************************************
***************defining control variable list********************
*****************************************************************


	   
global control1  /*sg_date_friday*/
       
global control2 lag_lossind log_mve lag_lev lag_btm lag_capex_pct fourthqtr_dum lag_roa qbhr /**size_qbhr**/  illiquidity volatil insti inside ln_cov /*ln_cov__67_90  ln_cov__12_90  ln_cov__16_90  ln_cov__6_90 ln_cov__1267_90 ln_cov  ln_cov2*/  /*disp__67_90  disp__12_90  disp__16_90 disp__6_90  disp__1267_90   disp disp2*/
       
gen mve = exp(log_mve)

cd "/zfs/projects/faculty/suzienoh-gps_proj/CleanData/"

export delimited using batch_NSZ_firm_adspender_20220912.csv, replace

	   

	   
	   
	   
	   
	   
	   
	   
	   

	 
	 
	
