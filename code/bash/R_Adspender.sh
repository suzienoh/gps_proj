#!/bin/bash

#SBATCH -J AdSpnd
#SBATCH -p normal
#SBATCH -c 15
#SBATCH --mem 450G
#SBATCH -t 2879:00
#SBATCH -o ./temp/R_AdSpender.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/apndxC/R_Adspender.R
Rscript code/Rscript/apndxC/R_Adspender.R