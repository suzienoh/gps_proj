#!/bin/bash

#SBATCH -J AdDig
#SBATCH -p normal
#SBATCH -c 15
#SBATCH --mem 650G
#SBATCH -t 2879:00
#SBATCH -o ./temp/R_AdDig.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/apndxC/R_AdDigital.R
Rscript code/Rscript/apndxC/R_AdDigital.R