#!/bin/bash

#SBATCH -J twt
#SBATCH -p normal
#SBATCH --array=1-66
#SBATCH -c 10
#SBATCH --mem 200G
#SBATCH -t 2879:00
#SBATCH -o ./temp/tweet/reg-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu


module load R/4.2.1

# RUN on dataset created from tweet_construct_vars_with_sentiment_and_like_retweet_thresholds2.R
echo Rscript code/Rscript/tweet/tweet_reg_sentiment_like_retweets2_3.R ${SLURM_ARRAY_TASK_ID} 2
Rscript code/Rscript/tweet/tweet_reg_sentiment_like_retweets2_3.R ${SLURM_ARRAY_TASK_ID} 2

# RUN on dataset created from tweet_construct_vars_with_sentiment_and_like_retweet_thresholds3.R
echo Rscript code/Rscript/tweet/tweet_reg_sentiment_like_retweets2_3.R ${SLURM_ARRAY_TASK_ID} 3
Rscript code/Rscript/tweet/tweet_reg_sentiment_like_retweets2_3.R ${SLURM_ARRAY_TASK_ID} 3