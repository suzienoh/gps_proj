################################################################################
## Script      : IR_run_winsor_reg.R
## Name        : Eddie Yu
## Description : This script runs regression given a dataframe.
##               Given 3 quintile indicators, it creates 2 subsamples by which
##               the quintile indicator = 5 or not, and produces .tex file
################################################################################


## Preamble, load necessary packages
rm(list = ls(all = TRUE))
library(data.table)
library(dplyr)
library(stargazer)
library(fixest)
library(car)
library(pastecs)
library(glue)
library(haven)
library(stringr)
library(statar)

## Set working directory
setwd("/zfs/projects/faculty/suzienoh-gps_proj/Eddie/gps_proj")

# command line arguments for parallel processing 14 different datasets
args <- commandArgs(trailingOnly = TRUE)
fileIndex <- as.numeric(args[1])
# fileIndex <- 11 # DELETE!

# read specific dataframe
saveFilePaths = c()
saveFilePrefix = "./data/out/IR_"
winsors = c("w0_")
winsors = append(winsors, paste0("w_", c( "yq_", "nai6_yq_", "nai2_yq_", "ym_", "nai6_ym_", "nai2_ym_")))
tiles   = c("xtile.csv", "ntile.csv")
tenure_restriction = 4

for (tile in tiles) {
  for (winsor in winsors) {
    outPath = paste0(saveFilePrefix, winsor, "t", tenure_restriction, "_", tile)
    print(outPath)
    saveFilePaths <- append(saveFilePaths, outPath)
  }
}

dataPath = saveFilePaths[fileIndex]
df <- fread(dataPath)


run_reg_xtile <- function(df, var, outtex, num_bins=5) {
  
  var = rlang::sym(var)
  print(glue("Quintile Variable : {var}"))
  
  print(glue("Saving filepath: {outtex}"))
  
  print(glue("Number of rows : {nrow(df)}"))
  
  setFixest_fml(..ctrl = ~lag_lossind + log_mve + lag_lev + lag_btm +
                  lag_capex_pct + fourthqtr_dum  + qbhr + volatil + ln_cov)
  
  
  df_high = df[eval(var) == num_bins, ]
  df_low  = df[eval(var) != num_bins, ]
  
  print(glue("Number of Rows: {nrow(df_high)} + {nrow(df_low)} = {nrow(df)}"))
  
  print(summary(df_high[is.finite(surp6_2), .(surp6_2, surp6_2_w, ym_surp6_2_w, yq_surp6_2_w, ywk_surp6_2_w, post, IR_tenure_i)]))
  print(summary(df_low[is.finite(surp6_2), .(surp6_2, surp6_2_w, ym_surp6_2_w, yq_surp6_2_w, ywk_surp6_2_w, post, IR_tenure_i)]))
  
  df_high = as.data.frame(df_high)
  df_low = as.data.frame(df_low)
  
  
  reg1 <- feols(ln_visits_by_day_w01 ~  post * IR_tenure_i + ..ctrl |
                  place_num + ym_eadate_cor^naics_code +
                  ym_eadate_cor^fips_county_hyb + dow_sg_date, df_high,
                cluster=~permno+ym_eadate_cor, fixef.rm = "both")
  
  reg2 <- feols(ln_visits_by_day_w01 ~  post * IR_tenure_i + ..ctrl |
                  place_num + ym_eadate_cor^naics_code +
                  ym_eadate_cor^fips_county_hyb + dow_sg_date, df_low,
                cluster=~permno+ym_eadate_cor, fixef.rm = "both")
  
  etable(reg1, reg2, file=outtex,
         replace=TRUE,
         digits = "r3", digits.stats = "r3",
         coefstat= "tstat", fitstat = ~ r2 + n)
  
}

# configuring output .tex filepath names
dataFileName = unlist(strsplit(dataPath, split = "/"))[4]
dataFileName = unlist(strsplit(dataFileName, split = ".csv"))
variable = "surp6_2_w"
prefixes = c("ym_", "yq_", "ywk_")

# run regression for 3 quintiles * 2 subsample, on Post*IR_tenure_i
for (var in paste0(prefixes, variable)) {
  outtex = paste0(glue("./out/tables/{dataFileName}_{var}.tex"))
  print(outtex)
  run_reg_xtile(df, var, outtex)
}

rm(list=ls())
gc()