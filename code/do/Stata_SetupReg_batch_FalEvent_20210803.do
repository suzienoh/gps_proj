clear all
set maxvar   32767, permanently
set more off, permanently

*************************************************************************************************************
/**Three areas specific to each file: "use ", "save", and "export"**/
*************************************************************************************************************


*use "/zfs/projects/faculty/suzienoh-gps_proj/CleanData/NSZ_yFIP_10_ne8k_allexcept202_20220807.dta", clear   /****/
use "/zfs/projects/faculty/suzienoh-gps_proj/CleanData/nsz_8ks1_day_ER_20220807.dta", clear    /******/
*use "/zfs/projects/faculty/suzienoh-gps_proj/CleanData/nsz_8ks1_day_NER1_20220807.dta", clear    /*********/





rename *, lower 


local event_date = "secadate"  /*8k*/



gen yr_event_date = year(`event_date')
gen qtr_event_date =quarter(`event_date')
gen m_event_date = month(`event_date')
gen yq_event_date = yr_event_date*10 + qtr_event_date
gen ym_event_date = yr_event_date*100 + m_event_date



****creating variables to be used as fixed effects in regressions****

gen double yqea_naic = yq_event_date*10000000 + naic  /*for year-quarter X industry fixed effects*/
gen double ymea_naic = ym_event_date*10000000 + naic  /*for year-quarter X industry fixed effects*/


gen double permno_yqact = yq_event_date*10000000  + permno /*for year-quarter X firm fixed effects*/
gen double permno_ymact = ym_event_date*10000000  + permno /*for year-quarter X firm fixed effects*/


egen place_num = group(safegraph_place_id)



gen double place_yqact = yq_event_date*100000000  + place_num /*for year-quarter X firm fixed effects*/
gen double place_ymact = ym_event_date*100000000  + place_num /*for year-quarter X firm fixed effects*/


 

egen region_num = group(region)

tabulate region
tabulate region_num, nolabel


gen double region_yqact = yq_event_date*1000  + region_num /*for year-quarter X region fixed effects*/
gen double region_ymact = ym_event_date*1000  + region_num /*for year-quarter X region fixed effects*/



egen city_num = group(city)


gen double city_yqact = yq_event_date*100000000  + city_num /*for year-quarter X region fixed effects*/
gen double city_ymact = ym_event_date*100000000  + city_num /*for year-quarter X region fixed effects*/



gen double fips_c_h_yqact = yq_event_date*100000000  + fips_county_hyb /*for year-quarter X region fixed effects*/
gen double fips_c_h_ymact = ym_event_date*100000000  + fips_county_hyb /*for year-quarter X region fixed effects*/

	


format /*yqea_ind48  ymea_ind48*/  yqea_naic  ymea_naic  permno_yqact  permno_ymact  place_yqact   place_ymact  region_yqact  region_ymact  city_yqact  city_ymact  fips_c_h_yqact  fips_c_h_ymact   %15.0g




cd "/zfs/projects/faculty/suzienoh-gps_proj/CleanData/"





 
 






/********logging daily visit counts*************/

local ccgrp "visits_by_day " /*visits_by_day_norm_cnty_hyb*/
local n : word count `ccgrp'
forvalues i = 1/`n' {
local a : word `i' of `ccgrp'

gen ln_`a'=ln(1+`a')

}







replace cov=0  if missing(cov)



gen ln_cov=ln(1+cov)



gen all =1




 
 

****drop subsidiaries****	
drop if d_parent_ticker==1















global base /*shrout*/ qbhr  size_qbhr /*sim_ea*/ illiquidity volatil insti inside inside_corr /*prccq*/ log_mve /*lossind */ lev btm capex_pct roa /*lag_lossind */ lag_lev lag_btm lag_capex_pct lag_roa /*fourthqtr_dum*/ /*friday*/  /*sg_date_friday*/  cov /*cov2*/ /*ln_cov__67_90*/ /*ln_cov__12_90*/  /*ln_cov__16_90*/  /*ln_cov__6_90*/ /*ln_cov__1267_90*/ ln_cov  /*ln_cov2*/  /*disp__67_90 */ /*disp__12_90*/  /*disp__16_90*/ /*disp__6_90*/  /*disp__1267_90*/ /*  disp disp2*/ /*distance_from_home*/    /*median_dwell*/ /*visits_per_visitor*/





winsor2   $base  , replace cuts(1 99) 





winsor2     visits_by_day   ln_visits_by_day     ,  suffix(_w001)  cuts(0.01 99.99) 
winsor2     visits_by_day   ln_visits_by_day   ,  suffix(_w01)  cuts(0.1 99.9) 
winsor2     visits_by_day   ln_visits_by_day    ,  suffix(_w1)  cuts(1 99) 









***************************************************************************
********creating "aggday" to capture days relative to the event date*******
********and keeping only 10 days before and after the event date***********
***************************************************************************

sort permno place_num `event_date' post sg_date  
by permno place_num `event_date' post: generate new_n = _n  /*first observation in pre starts from 1.*/  
replace new_n = new_n -1 if post==1 /*first observation in post starts from 0.*/ 



bysort permno place_num `event_date' post: egen post_max_new_n = max(new_n)


gen aggday = ((-1)*(post_max_new_n) + (new_n - 1)) if post==0

replace aggday = new_n if post==1





***************************************************************************
********EA[-7, +6]***********
***************************************************************************
drop if aggday >= 7
drop if aggday <= -8











*****************************************************************
***************creating variables needed for regressions*********
*****************************************************************


**day of week variables and fixed effects**
gen dow_sg_date = dow(sg_date)
tabulate  dow_sg_date, gen(dow_fe)


**firm-datatdate(fiscal quarter end) fixed effects**
gen permno_datadate = datadate*10000000  + permno 
   
	  

**creating an interactive term to be used later in regressions**

gen  Xvar_surp = .
	   
	   
	   
	   
	   
	   

cd "/zfs/projects/faculty/suzienoh-gps_proj/CleanData/"



/**don't revmoe this part*/
*save batch_NSZ_ne8k_allexcept202_20220808, replace  /**********/
save batch_NSZ_8ks1_day_ER_20220807, replace  /**********/
*save batch_NSZ_8ks1_day_NER1_20220807, replace  /********/





*export delimited using batch_8k_allexcept202_20220808.csv, replace  	   /*******/
export delimited using batch_8ks1_day_ER_20220807.csv, replace  	   /*******/
*export delimited using batch_8ks1_day_NER1_20220807.csv, replace  	    /*************/
  
	
	
	
 

 


