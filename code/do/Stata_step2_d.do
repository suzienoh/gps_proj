

clear all
set maxvar   32767, permanently
set more off, permanently

 

 
use "/zfs/projects/faculty/suzienoh-gps_proj/CleanData/batch_NSZ_census2_20211023.dta", clear     









cd "/zfs/projects/faculty/suzienoh-gps_proj/Stata_Results/"









***************************************************************************
********creating "aggday" to capture days relative to the event date*******
********and keeping only 10 days before and after the event date***********
***************************************************************************

sort permno place_num eadate post sg_date  
by permno place_num eadate post: generate new_n = _n  /*first observation in pre starts from 1.*/  
replace new_n = new_n -1 if post==1 /*first observation in post starts from 0.*/ 



bysort permno place_num eadate post: egen post_max_new_n = max(new_n)


gen aggday = ((-1)*(post_max_new_n) + (new_n - 1)) if post==0

replace aggday = new_n if post==1

drop if aggday >= 11 
drop if aggday <= -11















*****************************************************************
***************creating variables needed for regressions*********
*****************************************************************


**day of week variables and fixed effects**
gen dow_sg_date = dow(sg_date)
tabulate  dow_sg_date, gen(dow_fe)


**firm-datatdate(fiscal quarter end) fixed effects**
gen double permno_datadate = datadate*10000000  + permno 
format permno_datadate   %15.0g

   
   
	   

gen surp5 = ufe / abs(medest)
gen surp5_2 = ufe2 / abs(medest2)
gen surp5_67_90 = ufe__67_90  / abs(medest__67_90 ) 
gen surp5_12_90 = ufe__12_90  /  abs(medest__12_90)  
gen surp5_16_90 = ufe__16_90/  abs(medest__16_90 )
gen surp5_6_90 = ufe__6_90  / abs(medest__6_90)  
gen surp5_1267_90 =ufe__1267_90 / abs(medest__1267_90)




gen surp6 =ufe / disp
gen surp6_2 = ufe2 / disp2
gen surp6_67_90 = ufe__67_90  / disp__67_90  
gen surp6_12_90 = ufe__12_90  /  disp__12_90   
gen surp6_16_90 = ufe__16_90/  disp__16_90
gen surp6_6_90 = ufe__6_90  / disp__6_90  
gen surp6_1267_90 =ufe__1267_90 / disp__6_90  




winsor2   surp5 surp5_2 surp5_67_90 surp5_12_90  surp5_16_90  surp5_6_90  surp5_1267_90  surp6  surp6_2   surp6_67_90  surp6_12_90  surp6_16_90    surp6_6_90  surp6_1267_90,  suffix(_w1)  cuts(1 99) 



   
   
   
 capture drop   abs* sale_* ufe ufe2 ufe__67_90  ufe__12_90   ufe__16_90 ufe__6_90  ufe__1267_90 medest medest2  medest__67_90  medest__12_90   medest__16_90 medest__6_90  medest__1267_90 actual actual2 actual__67_90  actual__12_90   actual__16_90 actual__6_90  actual__1267_90 ea_aia* d_ea_aia* fqchg_sales chg_sales sales fscore
   
   
   
   
   
   
   
   
local ccgrp "surp6_2" 
local n : word count `ccgrp'
forvalues i = 1/`n' {
local a : word `i' of `ccgrp'

/* by X: */ egen `a'_four = xtile(`a'), n(4) 
/* by X: */ egen `a'_five = xtile(`a'), n(5) 
/* by X: */ egen `a'_six = xtile(`a'), n(6) 


 bysort ym_eadate:  egen m`a'_four = xtile(`a'), n(4) 
 bysort ym_eadate:  egen m`a'_five = xtile(`a'), n(5) 
 bysort ym_eadate:  egen m`a'_six = xtile(`a'), n(6) 


 bysort yq_eadate:  egen q`a'_four = xtile(`a'), n(4) 
 bysort yq_eadate:  egen q`a'_five = xtile(`a'), n(5) 
 bysort yq_eadate:  egen q`a'_six = xtile(`a'), n(6) 


 bysort ff_48:  egen ff`a'_four = xtile(`a'), n(4) 
 bysort ff_48:  egen ff`a'_five = xtile(`a'), n(5) 
 bysort ff_48:  egen ff`a'_six = xtile(`a'), n(6) 


 bysort ff_48 yq_eadate:  egen ffq`a'_four = xtile(`a'), n(4) 
 bysort ff_48 yq_eadate:  egen ffq`a'_five = xtile(`a'), n(5) 
 bysort ff_48 yq_eadate:  egen ffq`a'_six = xtile(`a'), n(6) 

 
 
 

tabulate `a'_four, gen(`a'four)
tabulate `a'_five, gen(`a'five)
tabulate `a'_six, gen(`a'six)



tabulate m`a'_four, gen(m`a'four)
tabulate m`a'_five, gen(m`a'five)
tabulate m`a'_six, gen(m`a'six)


tabulate q`a'_four, gen(q`a'four)
tabulate q`a'_five, gen(q`a'five)
tabulate q`a'_six, gen(q`a'six)


tabulate ff`a'_four, gen(ff`a'four)
tabulate ff`a'_five, gen(ff`a'five)
tabulate ff`a'_six, gen(ff`a'six)

tabulate ffq`a'_four, gen(ffq`a'four)
tabulate ffq`a'_five, gen(ffq`a'five)
tabulate ffq`a'_six, gen(ffq`a'six)
}	   


   
   
local ccgrp "surp6_2" 
local n : word count `ccgrp'
forvalues i = 1/`n' {
local a : word `i' of `ccgrp'
gen epos`a' = .
replace epos`a' = `a' if `a'>=0
gen pos`a' = .
replace pos`a' = `a' if `a'>0


gen neg`a' = .
replace neg`a' = `a' if `a'<0
gen eneg`a' = .
replace eneg`a' = `a' if `a'<=0

}	   



   
local ccgrp "surp6_2" 
local n : word count `ccgrp'
forvalues i = 1/`n' {
local a : word `i' of `ccgrp'

/* by X: */ egen pos`a'_three = xtile(pos`a'), n(3) 
/* by X: */ egen epos`a'_three = xtile(epos`a'), n(3) 
/* by X: */ egen neg`a'_three = xtile(neg`a'), n(3) 
/* by X: */ egen eneg`a'_three = xtile(eneg`a'), n(3) 

/* by X: */ egen pos`a'_four = xtile(pos`a'), n(4) 
/* by X: */ egen epos`a'_four = xtile(epos`a'), n(4) 
/* by X: */ egen neg`a'_four = xtile(neg`a'), n(4) 
/* by X: */ egen eneg`a'_four = xtile(eneg`a'), n(4) 

/* by X: */ egen pos`a'_five = xtile(pos`a'), n(5) 
/* by X: */ egen epos`a'_five = xtile(epos`a'), n(5) 
/* by X: */ egen neg`a'_five = xtile(neg`a'), n(5) 
/* by X: */ egen eneg`a'_five = xtile(eneg`a'), n(5) 


tabulate  pos`a'_three, gen(pos`a'three)
tabulate  epos`a'_three, gen(epos`a'three)
tabulate  neg`a'_three , gen(neg`a'three)
tabulate  eneg`a'_three , gen(eneg`a'three)

tabulate  pos`a'_four , gen(pos`a'four)
tabulate  epos`a'_four , gen(epos`a'four)
tabulate  neg`a'_four , gen(neg`a'four)
tabulate  eneg`a'_four , gen(eneg`a'four)

tabulate  pos`a'_five , gen(pos`a'five)
tabulate  epos`a'_five, gen(epos`a'five)
tabulate  neg`a'_five , gen(neg`a'five)
tabulate  eneg`a'_five , gen(eneg`a'five)



replace  pos`a'three1 =0 if !missing(pos`a') & missing(pos`a'three1)
replace  pos`a'three2 =0 if !missing(pos`a') & missing(pos`a'three2)
replace  pos`a'three3 =0 if !missing(pos`a') & missing(pos`a'three3)

replace  epos`a'three1 =0 if !missing(epos`a') &  missing(epos`a'three1)
replace  epos`a'three2 =0 if !missing(epos`a') &  missing(epos`a'three2)
replace  epos`a'three3 =0 if !missing(epos`a') &  missing(epos`a'three3)

replace  neg`a'three1 =0 if !missing(neg`a') &  missing(neg`a'three1)
replace  neg`a'three2 =0 if !missing(neg`a') &  missing(neg`a'three2)
replace  neg`a'three3 =0 if !missing(neg`a') &  missing(neg`a'three3)




replace  eneg`a'three1  =0 if !missing(eneg`a') &  missing(eneg`a'three1)
replace  eneg`a'three2  =0 if !missing(eneg`a') &  missing(eneg`a'three2)
replace  eneg`a'three3  =0 if !missing(eneg`a') &  missing(eneg`a'three3)







replace  pos`a'four1 =0 if !missing(pos`a') & missing(pos`a'four1)
replace  pos`a'four2 =0 if !missing(pos`a') & missing(pos`a'four2)
replace  pos`a'four3 =0 if !missing(pos`a') & missing(pos`a'four3)
replace  pos`a'four4 =0 if !missing(pos`a') & missing(pos`a'four4)



replace  epos`a'four1  =0 if !missing(epos`a') &  missing(epos`a'four1)
replace  epos`a'four2  =0 if !missing(epos`a') &  missing(epos`a'four2)
replace  epos`a'four3  =0 if !missing(epos`a') &  missing(epos`a'four3)
replace  epos`a'four4  =0 if !missing(epos`a') &  missing(epos`a'four4)




replace  neg`a'four1  =0 if !missing(neg`a') & missing(neg`a'four1)
replace  neg`a'four2  =0 if !missing(neg`a') & missing(neg`a'four2)
replace  neg`a'four3  =0 if !missing(neg`a') & missing(neg`a'four3)
replace  neg`a'four4  =0 if !missing(neg`a') & missing(neg`a'four4)




replace  eneg`a'four1  =0 if !missing(eneg`a') &  missing(eneg`a'four1)
replace  eneg`a'four2  =0 if !missing(eneg`a') &  missing(eneg`a'four2)
replace  eneg`a'four3  =0 if !missing(eneg`a') &  missing(eneg`a'four3)
*replace  eneg`a'four4  =0 if !missing(eneg`a') &  missing(eneg`a'four4)






replace  pos`a'five1  =0 if !missing(pos`a') &  missing(pos`a'five1)
replace  pos`a'five2  =0 if !missing(pos`a') &  missing(pos`a'five2)
replace  pos`a'five3  =0 if !missing(pos`a') &  missing(pos`a'five3)
replace  pos`a'five4  =0 if !missing(pos`a') &  missing(pos`a'five4)
replace  pos`a'five5  =0 if !missing(pos`a') &  missing(pos`a'five5)


replace  epos`a'five1 =0 if !missing(epos`a') &  missing(epos`a'five1)
replace  epos`a'five2 =0 if !missing(epos`a') &  missing(epos`a'five2)
replace  epos`a'five3 =0 if !missing(epos`a') &  missing(epos`a'five3)
replace  epos`a'five4 =0 if !missing(epos`a') &  missing(epos`a'five4)
replace  epos`a'five5 =0 if !missing(epos`a') &  missing(epos`a'five5)



replace  neg`a'five1 =0 if !missing(neg`a') &  missing(neg`a'five1)
replace  neg`a'five2 =0 if !missing(neg`a') &  missing(neg`a'five2)
replace  neg`a'five3 =0 if !missing(neg`a') &  missing(neg`a'five3)
replace  neg`a'five4 =0 if !missing(neg`a') &  missing(neg`a'five4)
replace  neg`a'five5 =0 if !missing(neg`a') &  missing(neg`a'five5)



replace  eneg`a'five1 =0 if !missing(eneg`a') &  missing(eneg`a'five1)
replace  eneg`a'five2 =0 if !missing(eneg`a') &  missing(eneg`a'five2)
replace  eneg`a'five3 =0 if !missing(eneg`a') &  missing(eneg`a'five3)
replace  eneg`a'five4 =0 if !missing(eneg`a') &  missing(eneg`a'five4)
*replace  eneg`a'five5 =0 if !missing(eneg`a') &  missing(eneg`a'five5)






}	   


 
 
   
local ccgrp "surp6_2" 
local n : word count `ccgrp'
forvalues i = 1/`n' {
local a : word `i' of `ccgrp'

gen X_`a'four1 = .
gen X_`a'four2 = .
gen X_`a'four3 =.
gen X_`a'four4 = .

gen X_`a'five1 = .
gen X_`a'five2 = .
gen X_`a'five3 = .
gen X_`a'five4 = .
gen X_`a'five5 = .

gen X_`a'six1 = .
gen X_`a'six2 = .
gen X_`a'six3 = .
gen X_`a'six4 = .
gen X_`a'six5 = .
gen X_`a'six6 = .


gen X_m`a'four1 = .
gen X_m`a'four2 = .
gen X_m`a'four3 = .
gen X_m`a'four4 = .


gen X_m`a'five1 = .
gen X_m`a'five2 = .
gen X_m`a'five3 = .
gen X_m`a'five4 = .
gen X_m`a'five5 = .

gen X_m`a'six1 = .
gen X_m`a'six2 = .
gen X_m`a'six3 = .
gen X_m`a'six4 = .
gen X_m`a'six5 = .
gen X_m`a'six6 = .

 

gen X_q`a'four1 = .
gen X_q`a'four2 = .
gen X_q`a'four3 = .
gen X_q`a'four4 = .


gen X_q`a'five1 =.
gen X_q`a'five2 = .
gen X_q`a'five3 = .
gen X_q`a'five4 = .
gen X_q`a'five5 =.

gen X_q`a'six1 = .
gen X_q`a'six2 = .
gen X_q`a'six3 = .
gen X_q`a'six4 = .
gen X_q`a'six5 = .
gen X_q`a'six6 = .



gen X_ff`a'four1 = .
gen X_ff`a'four2 =.
gen X_ff`a'four3 = .
gen X_ff`a'four4 =.


gen X_ff`a'five1 = .
gen X_ff`a'five2 =.
gen X_ff`a'five3 = .
gen X_ff`a'five4 = .
gen X_ff`a'five5 = .

gen X_ff`a'six1 = .
gen X_ff`a'six2 = .
gen X_ff`a'six3 = .
gen X_ff`a'six4 = .
gen X_ff`a'six5 = .
gen X_ff`a'six6 = .









gen X_ffq`a'four1 = .
gen X_ffq`a'four2 = .
gen X_ffq`a'four3 = .
gen X_ffq`a'four4 = .


gen X_ffq`a'five1 = .
gen X_ffq`a'five2 = .
gen X_ffq`a'five3 = .
gen X_ffq`a'five4 = .
gen X_ffq`a'five5 = .

gen X_ffq`a'six1 = .
gen X_ffq`a'six2 = .
gen X_ffq`a'six3 = .
gen X_ffq`a'six4 =.
gen X_ffq`a'six5 = .
gen X_ffq`a'six6 = .



 
gen X_pos`a'three1  =.
gen X_pos`a'three2 =.
gen X_pos`a'three3 =.
 

gen X_epos`a'three1 =.
gen X_epos`a'three2 =.
gen X_epos`a'three3 =.



gen X_neg`a'three1 =.
gen X_neg`a'three2 =.
gen X_neg`a'three3 =.


gen X_eneg`a'three1 =.
gen X_eneg`a'three2 =.
gen X_eneg`a'three3 =.




gen X_pos`a'four1 =.
gen X_pos`a'four2 =.
gen X_pos`a'four3 =.
gen X_pos`a'four4 =.


gen X_epos`a'four1 =.
gen X_epos`a'four2 =.
gen X_epos`a'four3 =.
gen X_epos`a'four4 =.



gen X_neg`a'four1 =.
gen X_neg`a'four2 =.
gen X_neg`a'four3 =.
gen X_neg`a'four4 =.


gen X_eneg`a'four1 =.
gen X_eneg`a'four2 =.
gen X_eneg`a'four3 =.
gen X_eneg`a'four4=.




gen X_pos`a'five1 =.
gen X_pos`a'five2 =.
gen X_pos`a'five3=.
gen X_pos`a'five4=.
gen X_pos`a'five5 =.


gen X_epos`a'five1 =.
gen X_epos`a'five2 =.
gen X_epos`a'five3 =.
gen X_epos`a'five4 =.
gen X_epos`a'five5 =.



gen X_neg`a'five1 =.
gen X_neg`a'five2 =.
gen X_neg`a'five3=.
gen X_neg`a'five4=.
gen X_neg`a'five5 =.


gen X_eneg`a'five1=.
gen X_eneg`a'five2 =.
gen X_eneg`a'five3 =.
gen X_eneg`a'five4 =.
gen X_eneg`a'five5=.






}	   

   
   
   
   
   
local ccgrp " surp6_2" 
local n : word count `ccgrp'
forvalues i = 1/`n' {
local a : word `i' of `ccgrp'
local Xvar post


replace X_`a'four1 = `Xvar' * `a'four1
replace X_`a'four2 = `Xvar' * `a'four2
replace X_`a'four3 = `Xvar' * `a'four3
replace X_`a'four4 = `Xvar' * `a'four4

replace X_`a'five1 = `Xvar' * `a'five1
replace X_`a'five2 = `Xvar' * `a'five2
replace X_`a'five3 = `Xvar' * `a'five3
replace X_`a'five4 = `Xvar' * `a'five4
replace X_`a'five5 = `Xvar' * `a'five5

replace X_`a'six1 = `Xvar' * `a'six1
replace X_`a'six2 = `Xvar' * `a'six2
replace X_`a'six3 = `Xvar' * `a'six3
replace X_`a'six4 = `Xvar' * `a'six4
replace X_`a'six5 = `Xvar' * `a'six5
replace X_`a'six6 = `Xvar' * `a'six6


replace X_m`a'four1 = `Xvar' * m`a'four1
replace X_m`a'four2 = `Xvar' * m`a'four2
replace X_m`a'four3 = `Xvar' * m`a'four3
replace X_m`a'four4 = `Xvar' * m`a'four4


replace X_m`a'five1 = `Xvar' * m`a'five1
replace X_m`a'five2 = `Xvar' * m`a'five2
replace X_m`a'five3 = `Xvar' * m`a'five3
replace X_m`a'five4 = `Xvar' * m`a'five4
replace X_m`a'five5 = `Xvar' * m`a'five5

replace X_m`a'six1 = `Xvar' * m`a'six1
replace X_m`a'six2 = `Xvar' * m`a'six2
replace X_m`a'six3 = `Xvar' * m`a'six3
replace X_m`a'six4 = `Xvar' * m`a'six4
replace X_m`a'six5 = `Xvar' * m`a'six5
replace X_m`a'six6 = `Xvar' * m`a'six6

 

replace X_q`a'four1 = `Xvar' * q`a'four1
replace X_q`a'four2 = `Xvar' * q`a'four2
replace X_q`a'four3 = `Xvar' * q`a'four3
replace X_q`a'four4 = `Xvar' * q`a'four4


replace X_q`a'five1 = `Xvar' * q`a'five1
replace X_q`a'five2 = `Xvar' * q`a'five2
replace X_q`a'five3 = `Xvar' * q`a'five3
replace X_q`a'five4 = `Xvar' * q`a'five4
replace X_q`a'five5 = `Xvar' * q`a'five5

replace X_q`a'six1 = `Xvar' * q`a'six1
replace X_q`a'six2 = `Xvar' * q`a'six2
replace X_q`a'six3 = `Xvar' * q`a'six3
replace X_q`a'six4 = `Xvar' * q`a'six4
replace X_q`a'six5 = `Xvar' * q`a'six5
replace X_q`a'six6 = `Xvar' * q`a'six6



replace X_ff`a'four1 = `Xvar' * ff`a'four1
replace X_ff`a'four2 = `Xvar' * ff`a'four2
replace X_ff`a'four3 = `Xvar' * ff`a'four3
replace X_ff`a'four4 = `Xvar' * ff`a'four4


replace X_ff`a'five1 = `Xvar' * ff`a'five1
replace X_ff`a'five2 = `Xvar' * ff`a'five2
replace X_ff`a'five3 = `Xvar' * ff`a'five3
replace X_ff`a'five4 = `Xvar' * ff`a'five4
replace X_ff`a'five5 = `Xvar' * ff`a'five5

replace X_ff`a'six1 = `Xvar' * ff`a'six1
replace X_ff`a'six2 = `Xvar' * ff`a'six2
replace X_ff`a'six3 = `Xvar' * ff`a'six3
replace X_ff`a'six4 = `Xvar' * ff`a'six4
replace X_ff`a'six5 = `Xvar' * ff`a'six5
replace X_ff`a'six6 = `Xvar' * ff`a'six6









replace X_ffq`a'four1 = `Xvar' * ffq`a'four1
replace X_ffq`a'four2 = `Xvar' * ffq`a'four2
replace X_ffq`a'four3 = `Xvar' * ffq`a'four3
replace X_ffq`a'four4 = `Xvar' * ffq`a'four4


replace X_ffq`a'five1 = `Xvar' * ffq`a'five1
replace X_ffq`a'five2 = `Xvar' * ffq`a'five2
replace X_ffq`a'five3 = `Xvar' * ffq`a'five3
replace X_ffq`a'five4 = `Xvar' * ffq`a'five4
replace X_ffq`a'five5 = `Xvar' * ffq`a'five5

replace X_ffq`a'six1 = `Xvar' * ffq`a'six1
replace X_ffq`a'six2 = `Xvar' * ffq`a'six2
replace X_ffq`a'six3 = `Xvar' * ffq`a'six3
replace X_ffq`a'six4 = `Xvar' * ffq`a'six4
replace X_ffq`a'six5 = `Xvar' * ffq`a'six5
replace X_ffq`a'six6 = `Xvar' * ffq`a'six6











 
replace X_pos`a'three1 = `Xvar' * pos`a'three1
replace X_pos`a'three2 = `Xvar' * pos`a'three2
replace X_pos`a'three3 = `Xvar' * pos`a'three3
 

replace X_epos`a'three1 = `Xvar' * epos`a'three1
replace X_epos`a'three2 = `Xvar' * epos`a'three2
replace X_epos`a'three3 = `Xvar' * epos`a'three3



replace X_neg`a'three1 = `Xvar' * neg`a'three1
replace X_neg`a'three2 = `Xvar' * neg`a'three2
replace X_neg`a'three3 = `Xvar' * neg`a'three3


replace X_eneg`a'three1 = `Xvar' * eneg`a'three1
replace X_eneg`a'three2 = `Xvar' * eneg`a'three2
replace X_eneg`a'three3 = `Xvar' * eneg`a'three3




replace X_pos`a'four1 = `Xvar' * pos`a'four1
replace X_pos`a'four2 = `Xvar' * pos`a'four2
replace X_pos`a'four3 = `Xvar' * pos`a'four3
replace X_pos`a'four4 = `Xvar' * pos`a'four4


replace X_epos`a'four1 = `Xvar' * epos`a'four1
replace X_epos`a'four2 = `Xvar' * epos`a'four2
replace X_epos`a'four3 = `Xvar' * epos`a'four3
replace X_epos`a'four4 = `Xvar' * epos`a'four4



replace X_neg`a'four1 = `Xvar' * neg`a'four1
replace X_neg`a'four2 = `Xvar' * neg`a'four2
replace X_neg`a'four3 = `Xvar' * neg`a'four3
replace X_neg`a'four4 = `Xvar' * neg`a'four4


replace X_eneg`a'four1 = `Xvar' * eneg`a'four1
replace X_eneg`a'four2 = `Xvar' * eneg`a'four2
replace X_eneg`a'four3 = `Xvar' * eneg`a'four3
*replace X_eneg`a'four4 = `Xvar' * eneg`a'four4





replace X_pos`a'five1 = `Xvar' * pos`a'five1
replace X_pos`a'five2 = `Xvar' * pos`a'five2
replace X_pos`a'five3 = `Xvar' * pos`a'five3
 replace X_pos`a'five4 = `Xvar' * pos`a'five4
replace X_pos`a'five5 = `Xvar' * pos`a'five5


replace X_epos`a'five1 = `Xvar' * epos`a'five1
replace X_epos`a'five2 = `Xvar' * epos`a'five2
replace X_epos`a'five3 = `Xvar' * epos`a'five3
replace X_epos`a'five4 = `Xvar' * epos`a'five4
replace X_epos`a'five5 = `Xvar' * epos`a'five5



replace X_neg`a'five1 = `Xvar' * neg`a'five1
replace X_neg`a'five2 = `Xvar' * neg`a'five2
replace X_neg`a'five3 = `Xvar' * neg`a'five3
replace X_neg`a'five4 = `Xvar' * neg`a'five4
replace X_neg`a'five5 = `Xvar' * neg`a'five5


replace X_eneg`a'five1 = `Xvar' * eneg`a'five1
replace X_eneg`a'five2 = `Xvar' * eneg`a'five2
replace X_eneg`a'five3 = `Xvar' * eneg`a'five3
replace X_eneg`a'five4 = `Xvar' * eneg`a'five4
*replace X_eneg`a'five5 = `Xvar' * eneg`a'five5







}	   





   
   
   
 keep if d_parent_ticker==0



cd "/zfs/projects/faculty/suzienoh-gps_proj/CleanData/"
 

	   

export delimited using batch_NSZ_census2_20211023.csv, replace  
	   
   
	   
	   exit
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   

	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   
	   


