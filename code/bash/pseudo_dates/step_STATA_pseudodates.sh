#!/bin/bash

#SBATCH -J pseudo
#SBATCH -p normal
#SBATCH -c 10
#SBATCH -n 1
#SBATCH --mem 799G
#SBATCH -t 2879:00
#SBATCH -o ./temp/step_STATA_pseudo.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/pseudo_dates/step_STATA_pseudodates.R 
Rscript code/Rscript/pseudo_dates/step_STATA_pseudodates.R