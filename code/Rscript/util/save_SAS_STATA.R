################################################################################
## Script      : save_SAS_STATA.R
## Name        : Eddie Yu
## Description : This script combines & converts STATA_step1_a.do ~ STATA_step1_g.do
#                AND STATA_step2_a ~ STATA_step2_f.do to make a SINGLE, COMBINED,
##               MODULARIZED Rscript. Indexing the csv files (a~g) will run the same
##               code in R instead of STATA.
################################################################################


## Preamble, load necessary packages
rm(list = ls(all = TRUE))
library(data.table)
library(dplyr)
library(stargazer)
library(tidyverse)
library(fixest)
library(car)
library(DescTools)
library(glue)
library(haven)

## Set working directory
setwd('/zfs/projects/faculty/suzienoh-gps_proj/Eddie/gps_proj')


# get index a~g from command line
args <- commandArgs(trailingOnly = TRUE)
fileIndex <- as.numeric(args[1])

################################################################################
# 1. Configure Input / Output Filepath, Read Input data
################################################################################
filedict = c("../../CleanData/NSZ_yFIP_10_firm_nabs_20211023.dta",
                "../../CleanData/NSZ_yFIP_10_firm_abs_20211023.dta",
                "../../CleanData/NSZ_yFIP_10_census1_20211023.dta",
                "../../CleanData/NSZ_yFIP_10_census2_20211023.dta",
                "../../CleanData/NSZ_yFIP_10_RP_20211023.dta",
                "../../CleanData/NSZ_TESTIND_20220717.dta",
                "../../CleanData/batch_NSZ_compqa_ea_qbhr_2022.dta")

outPathdict = c("./data/out/NSZ_yFIP_10_firm_nabs_20211023.csv",
                   "./data/out/NSZ_yFIP_10_firm_abs_20211023.csv",
                   "./data/out/NSZ_yFIP_10_census1_20211023.csv",
                   "./data/out/NSZ_yFIP_10_census2_20211023.csv",
                   "./data/out/NSZ_yFIP_10_RP_20211023.csv",
                   "./data/out/NSZ_TESTIND_20220717.csv",
                   "./data/out/batch_NSZ_compqa_ea_qbhr_2022.csv")



dataFilePath <- filedict[fileIndex]
outPath      <- outPathdict[fileIndex]
print(glue('Running File Index: {fileIndex} / Input Data Path: {dataFilePath} / Output File Path: {outPath}'))

df <- setDT(read_stata(dataFilePath))
fwrite(df, outPath)

print(glue("Done Writing to {outPath}"))
quit(save = "no")