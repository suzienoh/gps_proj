#!/bin/bash

#SBATCH -J census
#SBATCH -p normal
#SBATCH --array=1-2
#SBATCH -c 10
#SBATCH --mem 250G
#SBATCH -t 300:00
#SBATCH -o ./temp/census/filter-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu


module load R/4.2.1
# dataIndex=1 # array=1-17 for dataIndex=1 & dataIndex=2
echo Rscript code/Rscript/census/filter_census_data.R ${SLURM_ARRAY_TASK_ID}
Rscript code/Rscript/census/filter_census_data.R ${SLURM_ARRAY_TASK_ID}