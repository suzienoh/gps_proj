#!/bin/bash

#SBATCH -J RP1cal
#SBATCH -p normal
#SBATCH -c 30
#SBATCH --mem 200G
#SBATCH -t 2879:00
#SBATCH -o ./temp/RP1cal.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/table4/R_RP1cal_hardcodeFE.R
Rscript code/Rscript/table4/R_RP1cal_hardcodeFE.R