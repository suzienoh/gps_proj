#!/bin/bash

#SBATCH -J sp1day
#SBATCH -p normal
#SBATCH -c 10
#SBATCH --mem 80G
#SBATCH -t 2879:00
#SBATCH -o ./temp/sp_reaction/reg-day1-sp_.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/sp_reaction/reg_day1_sp_.R
Rscript code/Rscript/sp_reaction/reg_day1_sp_.R