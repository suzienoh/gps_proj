#!/bin/bash

#SBATCH -J twt
#SBATCH -p normal
#SBATCH -c 10
#SBATCH --array=1-2
#SBATCH --mem 120G
#SBATCH -t 2879:00
#SBATCH -o ./temp/tweet/create_nsz_abs_tweet_ea1w_data-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu


module load R/4.2.1
if [ ${SLURM_ARRAY_TASK_ID} -eq 1 ]
then
bool='T'

elif [ ${SLURM_ARRAY_TASK_ID} -eq 2 ]
then
bool='F'
fi

echo Rscript code/Rscript/tweet/create_nsz_abs_tweet_ea1w_data.R $bool
Rscript code/Rscript/tweet/create_nsz_abs_tweet_ea1w_data.R $bool