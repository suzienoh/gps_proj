#!/bin/bash

#SBATCH -J attnt
#SBATCH -p normal
#SBATCH --array=1-2
#SBATCH -c 10
#SBATCH --mem 100G
#SBATCH -t 2879:00
#SBATCH -o ./temp/tweet/make-tweet-attnt-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu


module load R/4.2.1
if [ ${SLURM_ARRAY_TASK_ID} -eq 1 ]
then
bool='T'

elif [ ${SLURM_ARRAY_TASK_ID} -eq 2 ]
then
bool='F'
fi


echo Rscript code/Rscript/tweet/make_data_tweet_attnt.R $bool
Rscript code/Rscript/tweet/make_data_tweet_attnt.R $bool