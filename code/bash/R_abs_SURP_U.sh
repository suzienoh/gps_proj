#!/bin/bash

#SBATCH -J R_absSURP
#SBATCH -p normal
#SBATCH --array=1-3
#SBATCH -c 10
#SBATCH --mem 300G
#SBATCH -t 2879:00
#SBATCH -o ./temp/R_absSURP-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/table5/R_abs_SURP_U_hardcodeFE.R ${SLURM_ARRAY_TASK_ID}
Rscript code/Rscript/table5/R_abs_SURP_U_hardcodeFE.R ${SLURM_ARRAY_TASK_ID}
