#!/bin/bash

#SBATCH -J save
#SBATCH -p normal
#SBATCH --array=2
#SBATCH -c 20
#SBATCH --mem 300G
#SBATCH -t 300:00
#SBATCH -o ./temp/save-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/util/save_partial_data.R ${SLURM_ARRAY_TASK_ID}
Rscript code/Rscript/util/save_partial_data.R ${SLURM_ARRAY_TASK_ID}