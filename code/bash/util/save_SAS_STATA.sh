#!/bin/bash

#SBATCH -J SAS
#SBATCH -p normal
#SBATCH --array=3-7
#SBATCH -c 10
#SBATCH --mem 400G
#SBATCH -t 2879:00
#SBATCH -o ./temp/save-sas-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu


module load R/4.2.1
echo Rscript code/Rscript/util/save_SAS_STATA.R ${SLURM_ARRAY_TASK_ID}
Rscript code/Rscript/util/save_SAS_STATA.R ${SLURM_ARRAY_TASK_ID}