#!/bin/bash

#SBATCH -J R_NameSim
#SBATCH -p normal
#SBATCH -c 10
#SBATCH --mem 200G
#SBATCH -t 2879:00
#SBATCH -o ./temp/R_NameSim.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/table7/R_NameSimilarity_store_hardcodeFE.R
Rscript code/Rscript/table7/R_NameSimilarity_store_hardcodeFE.R
