#!/bin/bash

#SBATCH -J pm2
#SBATCH -p normal
#SBATCH -c 10
#SBATCH -n 1
#SBATCH --mem 799G
#SBATCH -t 2879:00
#SBATCH -o ./temp/pseudo_pm2.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/pseudo_dates/pm2/construct_pm2.R
Rscript code/Rscript/pseudo_dates/pm2/construct_pm2.R