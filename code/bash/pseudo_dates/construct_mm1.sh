#!/bin/bash

#SBATCH -J mm1
#SBATCH -p normal
#SBATCH -c 10
#SBATCH -n 1
#SBATCH --mem 799G
#SBATCH -t 2879:00
#SBATCH -o ./temp/pseudo_mm1.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/pseudo_dates/mm1/construct_mm1.R
Rscript code/Rscript/pseudo_dates/mm1/construct_mm1.R