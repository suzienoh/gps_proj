#!/bin/bash

#SBATCH -J safegph
#SBATCH -p normal
#SBATCH -c 10
#SBATCH --mem 100G
#SBATCH -t 2879:00
#SBATCH -o ./temp/clean_safegph.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

# Load software
loaded modules
source deactivate
module purge
conda deactivate

module load R/4.2.1
echo Rscript code/Rscript/util/clean_Safegraph_adittional_processed.R
Rscript code/Rscript/util/clean_Safegraph_adittional_processed.R