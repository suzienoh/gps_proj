
rm(list = ls(all = TRUE))
  
## Preamble, set wd and load necessary packages
setwd('/zfs/projects/faculty/suzienoh-gps_proj/CleanData/')
library(fixest)



library(foreach)
library(doParallel)
library(data.table)
library(car)
library(dplyr)   








leftd <- fread('/zfs/projects/faculty/suzienoh-gps_proj/CleanData/batch_NSZ_firm_nabs_20211023.csv',header=TRUE, stringsAsFactors=FALSE) 
leftd <- data.frame(leftd)

rightd <- fread('/zfs/projects/faculty/suzienoh-gps_proj/CleanData/Firms224_CZ.csv',header=TRUE, stringsAsFactors=FALSE)
rightd <- data.frame(rightd)

data<-merge(x=leftd,y=rightd[ , c("permno", "high_price", "retail_dining", "high_price_industry")],by="permno",all.x=TRUE)

tail(data [,colnames(data ) %in% c("permno", "high_price", "retail_dining", "high_price_industry")], n=100) 
 







data$ym_eadate_cor = data$yr_eadate*100 + data$m_eadate
data <-data[data$d_parent_ticker==0,]




    
# Defining the macro variables
setFixest_fml(..ctrl = ~lag_lossind + log_mve + lag_lev + lag_btm + lag_capex_pct + fourthqtr_dum  + qbhr + volatil + ln_cov) 

     



data$high_price_industry01 <- ifelse(is.na(data$high_price_industry), 0, 1)


 

data <-data[between(data$aggday, -7, 6),]
  




 
         
data$lnn1_visits_by_day_w1 = log(data$visits_by_day_w1)
data$lnn1_visits_by_day_w01 = log(data$visits_by_day_w01)
data$lnn1_visits_by_day_w001 = log(data$visits_by_day_w001)
   
    
    
 
data <- data %>% mutate(five_surp6_2 = ntile(surp6_2, 5))
data <- data %>% group_by(ym_eadate_cor) %>% mutate(five_ym_surp6_2 = ntile(surp6_2, 5))
data <- data %>% group_by(yq_eadate) %>% mutate(five_yq_surp6_2 = ntile(surp6_2, 5))



data$f5_surp6_2 <- ifelse((data$five_surp6_2==5), 1, 0)
data$f4_surp6_2 <- ifelse((data$five_surp6_2==4), 1, 0)
data$f3_surp6_2 <- ifelse((data$five_surp6_2==3), 1, 0)
data$f2_surp6_2 <- ifelse((data$five_surp6_2==2), 1, 0)
data$f1_surp6_2 <- ifelse((data$five_surp6_2==1), 1, 0)



data$f5_surp6_2 <- ifelse(is.na(data$five_surp6_2), NA, data$f5_surp6_2)
data$f4_surp6_2 <- ifelse(is.na(data$five_surp6_2), NA, data$f4_surp6_2)
data$f3_surp6_2 <- ifelse(is.na(data$five_surp6_2), NA, data$f3_surp6_2)
data$f2_surp6_2 <- ifelse(is.na(data$five_surp6_2), NA, data$f2_surp6_2)
data$f1_surp6_2 <- ifelse(is.na(data$five_surp6_2), NA, data$f1_surp6_2)


       
data$post_f5_surp6_2 = data$post * data$f5_surp6_2       
data$post_f4_surp6_2 = data$post * data$f4_surp6_2       
data$post_f3_surp6_2 = data$post * data$f3_surp6_2       
data$post_f2_surp6_2 = data$post * data$f2_surp6_2       
data$post_f1_surp6_2 = data$post * data$f1_surp6_2       


       

data$post_high_price_industry01 = data$post * data$high_price_industry01



data <- data[order(data$eadate),]

tail(data [,colnames(data ) %in% c("eadate")], n=50) 
head(data [,colnames(data ) %in% c("eadate")], n=50) 
 
 
 
 

reg1 <- feols(ln_visits_by_day_w01 ~ surp6_2five2 + surp6_2five3 + surp6_2five4 + surp6_2five5 + X_surp6_2five1 + X_surp6_2five2 + X_surp6_2five3 + X_surp6_2five4 + X_surp6_2five5 + ..ctrl  | place_num + ym_eadate_cor^naics_code + ym_eadate_cor^fips_county_hyb + dow_sg_date, data, cluster=~permno+ym_eadate_cor, fixef.rm = "both")

etable(  reg1, 
       file='/zfs/projects/faculty/suzienoh-gps_proj/Stata_Results/surp6_2five_all_20200109.tex', replace = TRUE, digits = "r3", digits.stats = "r3",
        coefstat= "tstat", fitstat = ~ r2 + n)
       
       
       



 
reg2 <- feols(ln_visits_by_day_w01 ~ post + ..ctrl | place_num + ym_eadate_cor^naics_code + ym_eadate_cor^fips_county_hyb + dow_sg_date, data[data$high_price_industry01==1,], cluster=~permno+ym_eadate_cor, fixef.rm = "both")


reg3 <- feols(ln_visits_by_day_w01 ~ post + ..ctrl | place_num + ym_eadate_cor^naics_code + ym_eadate_cor^fips_county_hyb + dow_sg_date, data[data$high_price_industry01==0,], cluster=~permno+ym_eadate_cor, fixef.rm = "both")


etable( reg2, reg3, 
       file='/zfs/projects/faculty/suzienoh-gps_proj/Stata_Results/post_industry_20200109.tex', replace = TRUE, digits = "r3", digits.stats = "r3",
        coefstat= "tstat", fitstat = ~ r2 + n)
 





reg2 <- feols(ln_visits_by_day_w01 ~ surp6_2five2 + surp6_2five3 + surp6_2five4 + surp6_2five5 + X_surp6_2five1 + X_surp6_2five2 + X_surp6_2five3 + X_surp6_2five4 + X_surp6_2five5 + ..ctrl  | place_num + ym_eadate_cor^naics_code + ym_eadate_cor^fips_county_hyb + dow_sg_date, data[data$high_price_industry01==1,], cluster=~permno+ym_eadate_cor, fixef.rm = "both")
reg3 <- feols(ln_visits_by_day_w01 ~ surp6_2five2 + surp6_2five3 + surp6_2five4 + surp6_2five5 + X_surp6_2five1 + X_surp6_2five2 + X_surp6_2five3 + X_surp6_2five4 + X_surp6_2five5 + ..ctrl | place_num + ym_eadate_cor^naics_code + ym_eadate_cor^fips_county_hyb + dow_sg_date, data[data$high_price_industry01==0,], cluster=~permno+ym_eadate_cor, fixef.rm = "both")


etable(  reg2, reg3, 
       file='/zfs/projects/faculty/suzienoh-gps_proj/Stata_Results/surp6_2five_bigticket_20200109.tex', replace = TRUE, digits = "r3", digits.stats = "r3",
        coefstat= "tstat", fitstat = ~ r2 + n)
       
       
       
       
       
       
       