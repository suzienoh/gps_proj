#!/bin/bash

#SBATCH -J R_8K
#SBATCH -p normal
#SBATCH --array=1-3
#SBATCH -c 30
#SBATCH --mem 200G
#SBATCH -t 2879:00
#SBATCH -o ./temp/R_8K-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/table8/R_8K_hardcodeFE.R ${SLURM_ARRAY_TASK_ID}
Rscript code/Rscript/table8/R_8K_hardcodeFE.R ${SLURM_ARRAY_TASK_ID}
