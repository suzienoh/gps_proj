#!/bin/bash

#SBATCH -J mm2_mm2_1
#SBATCH -p normal
#SBATCH -c 10
#SBATCH --array=1-4
#SBATCH --mem 120G
#SBATCH -t 2879:00
#SBATCH -o ./temp/pseudo-mm2_mm2_1-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/pseudo_dates/mm2/R_mm2_run_mm2_1.R ${SLURM_ARRAY_TASK_ID}
Rscript code/Rscript/pseudo_dates/mm2/R_mm2_run_mm2_1.R ${SLURM_ARRAY_TASK_ID}