#!/bin/bash

#SBATCH -J sp_ext
#SBATCH -p normal
#SBATCH -c 10
#SBATCH --mem 300G
#SBATCH -t 250:00
#SBATCH -o ./temp/sp_react.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu


module load R/4.2.1
echo Rscript code/Rscript/sp_reaction/sp_reaction_construct.R
Rscript code/Rscript/sp_reaction/sp_reaction_construct.R