#!/bin/bash

#SBATCH -J census2
#SBATCH -p normal
#SBATCH -c 5
#SBATCH --mem 250G
#SBATCH -t 2879:00
#SBATCH -o ./temp/census/run-census2-loop.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/census/demographic_census2_loop.R
Rscript code/Rscript/census/demographic_census2_loop.R