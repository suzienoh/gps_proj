# Initialize the concatenated CSV file with the header from the first file
head -1 data/out/census/R_batch_NSZ_census1_20211023_partial_1.csv > data/out/census/R_batch_NSZ_census1_20211023_partial_all.csv

# Concatenate data from files 1 to 17
for i in $(seq 1 1 17)
do
    filename="data/out/census/R_batch_NSZ_census1_20211023_partial_${i}.csv"
    
    # Check if the file exists before concatenating
    if [ -e "$filename" ]
    then
        wc -l "$filename"
        # read in data w/o header
        sed 1d "$filename" >> data/out/census/R_batch_NSZ_census1_20211023_partial_all.csv
    else
        echo "File $filename does not exist."
    fi
done