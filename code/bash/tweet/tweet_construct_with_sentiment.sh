#!/bin/bash

#SBATCH -J twt
#SBATCH -p normal
#SBATCH -c 10
#SBATCH --mem 300G
#SBATCH -t 100:00
#SBATCH -o ./temp/tweet/construct3.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu


module load R/4.2.1
# Following is for only selecting [EA-1, EA+1] tweets ( 19610 (49%) of 39,305 tweets are dropped); filters out empty tweets too
# echo Rscript code/Rscript/tweet/tweet_construct_vars_with_sentiment_and_like_retweet_thresholds.R
# Rscript code/Rscript/tweet/tweet_construct_vars_with_sentiment_and_like_retweet_thresholds.R

# Following is for only sifting out empty tweets ( 2385 (8%) of 39,305 tweets are dropped)
# echo Rscript code/Rscript/tweet/tweet_construct_vars_with_sentiment_and_like_retweet_thresholds2.R
# Rscript code/Rscript/tweet/tweet_construct_vars_with_sentiment_and_like_retweet_thresholds2.R

# Following is for only sifting out [EA-1, EA+1] tweets ( 17310 (43%) of 39,305 tweets are dropped); filters out empty tweets too
echo Rscript code/Rscript/tweet/tweet_construct_vars_with_sentiment_and_like_retweet_thresholds3.R
Rscript code/Rscript/tweet/tweet_construct_vars_with_sentiment_and_like_retweet_thresholds3.R