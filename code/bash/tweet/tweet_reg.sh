#!/bin/bash

#SBATCH -J tweet
#SBATCH -p normal
#SBATCH --array=1-4
#SBATCH -c 10
#SBATCH --mem 100G
#SBATCH -t 2879:00
#SBATCH -o ./temp/tweet-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu


module load R/4.2.1
echo Rscript code/Rscript/tweet/tweet_reg.R ${SLURM_ARRAY_TASK_ID}
Rscript code/Rscript/tweet/tweet_reg.R ${SLURM_ARRAY_TASK_ID}