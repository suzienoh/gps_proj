#!/bin/bash

#SBATCH -J IR_reg
#SBATCH -p normal
#SBATCH --array=1-14
#SBATCH -c 10
#SBATCH --mem 100G
#SBATCH -t 2879:00
#SBATCH -o ./temp/IR-reg-%j-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
echo Rscript code/Rscript/IR/IR_run_winsor_reg.R ${SLURM_ARRAY_TASK_ID}
Rscript code/Rscript/IR/IR_run_winsor_reg.R ${SLURM_ARRAY_TASK_ID}