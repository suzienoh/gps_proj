#!/bin/bash

#SBATCH -J lag_notif
#SBATCH -p normal
#SBATCH --array=1-21
#SBATCH -c 10
#SBATCH --mem 80G
#SBATCH -t 2879:00
#SBATCH -o ./temp/earn_notif/notif_lag-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu


module load R/4.2.1
echo Rscript code/Rscript/earn_notif/earn_notif_lag_reg.R ${SLURM_ARRAY_TASK_ID}
Rscript code/Rscript/earn_notif/earn_notif_lag_reg.R ${SLURM_ARRAY_TASK_ID}