#!/bin/bash

#SBATCH -J mm1_mw2
#SBATCH -p normal
#SBATCH -c 10
#SBATCH --array=1-2
#SBATCH --mem 100G
#SBATCH -t 2879:00
#SBATCH -o ./temp/pseudo-mm1_mw2-%a.out
#SBATCH --mail-type=ALL
#SBATCH --mail-user=jisangyu@stanford.edu

module load R/4.2.1
if [ ${SLURM_ARRAY_TASK_ID} == 1 ]
then
bool="T"
echo $bool

elif [ ${SLURM_ARRAY_TASK_ID} == 2 ]
then
bool="F"
echo $bool
fi

echo Rscript code/Rscript/pseudo_dates/mm1/R_mm1_run_mw2.R $bool
Rscript code/Rscript/pseudo_dates/mm1/R_mm1_run_mw2.R $bool